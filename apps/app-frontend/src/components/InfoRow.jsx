import React from 'react'
function InfoRow(props) {
    const {name,value} = props
    return (
        <div className="row">
            <div className="col-lg-6">
                <label>{name}:</label>
            </div>
            <div className="col-lg-6">
                <p>{value}</p>
            </div>
        </div>
    )
}
export default InfoRow