import React from 'react'
import Alert from 'react-bootstrap/Alert'
let isAlertShow = false
let alertMessage = 'All is right'
let alertType = 'primary'

export class CustomAlert extends React.Component {
    
    constructor(props){
        super(props);
      }; 
      componentDidUpdate (prevProps) {
        if(prevProps.alertIsShow !== this.props.alertIsShow)
        setTimeout(() => this.props.close(), 4000)
      }
    render () {
        return (
            <Alert show='true' 
            key='mainAlert' variant={this.props.alert.type}>
              {this.props.alert.message}
            </Alert>
        )
    }
}