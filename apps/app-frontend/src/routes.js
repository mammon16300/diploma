import Root from "./modules/Root/container/Root";
import Login from "./modules/Login/container/Login";
import AdminMenu from "./modules/AdminMenu/container/AdminMenu"
import UserRegistration from "./modules/Users/container/UserRegistration";
import UserList from "./modules/Users/container/UserList";
import UserInfo from "./modules/Users/container/UserInfo";
import Subscription from './modules/Subscription/container/Subscription'
import ProductsList from './modules/Products/container/ProductsList'
import NewProduct from './modules/Products/container/NewProduct'
import GymSessions from './modules/GymSessions/container/GymSessions'
import SubscriptionOrder from './modules/Order/container/SubscriptionOrder'

import Trainings from './modules/Trainings/container/Trainings'
import UserSubscriptions from './modules/Users/container/UerSubscriptions'
import PersonalTrainingsOrder from './modules/Order/container/PersonalTrainingsOrder'
import AdminDahboard from "./modules/Dashboard/container/AdminDahboard";

const routes = [
  {
    name: 'Адмін панель',
    path: '/dashboard',
    component: AdminDahboard,
    layout: '',
    permisionsFor: ['admin']
  },
  {
    name: 'Сесії клієнтів',
    path: '/gymsessions',
    component: GymSessions,
    layout: '',
    permisionsFor: ['manager', 'admin']
  },  
  {
    name: 'Товари',
    path: '/products',
    layout: '/',
    permisionsFor: ['manager', 'admin'],
    childs: [
      {
        name: 'Список товарів',
        path: '/list',
        component: ProductsList,
        layout: '/products',
        permisionsFor: ['manager', 'admin']
      },
      {
        name: 'Додати новий товар',
        path: '/new',
        component: NewProduct,
        layout: '/products',
        permisionsFor: ['manager', 'admin']
      },
    ]
  },
  {
    name: 'Абонементи',
    path: '/subscription',
    component: Subscription,
    layout: '/',
    permisionsFor: ['manager', 'admin'],
    childs: [
      {
        name: 'Список',
        path: '/list',
        component: Subscription,
        layout: '/subscription',
        permisionsFor: ['manager', 'admin'],
      },
      {
        name: 'Створити',
        path: '/create',
        component: Subscription,
        layout: '/subscription',
        permisionsFor: ['manager', 'admin'],
      }
    ]
  },
      {
        path: '/info',
        component: UserInfo,
        name: 'Профіль',
        layout: '',
        permisionsFor: ['user', 'coach']
      },
      {
        path: `/trainings`,
        name: 'Графік тренувань',
        component: Trainings,
        layout: '',
        permisionsFor: ['user', 'coach']
      },
      {
        path: `/subscriptions`,
        name: 'Абонементи',
        component: UserSubscriptions,
        layout: '',
        permisionsFor: ['user', 'coach']
      },
      {
        path: `/subscriptionorder`,
        name: 'Оформити Абонемент',
        component: SubscriptionOrder,
        layout: '',
        permisionsFor: ['user', 'coach']
      } ,   
      {
        path: `/trainingsonorder`,
        name: 'Купити особисті тренування',
        component: PersonalTrainingsOrder,
        layout: '',
        permisionsFor: ['user']
      } ,  
      {
        name: 'Список клієнтів',
        path: '/list',
        component: UserList,
        layout: '/user',
        permisionsFor: ['coach']
      },
  {
    name: 'Користувачі',
    path: '/user',
    layout: '/',
    permisionsFor: ['manager', 'admin'],
    childs: [
      {
        name: 'Реєстрація користувача',
        path: '/registration',
        component: UserRegistration,
        layout: '/user',
        permisionsFor: ['manager', 'admin']
      },
      {
        name: 'Список користувачів',
        path: '/list',
        component: UserList,
        layout: '/user',
        permisionsFor: ['manager', 'admin']
      },
      {
        name: 'Профіль',
        path: '/info',
        component: UserInfo,
        layout: '/user',
        permisionsFor: ['manager', 'admin']
      }
    ]
  }
];
export default routes;