import React, { Component } from 'react'
import UserInfo from '../container/UserInfo'
import UserEdit from '../../Users/container/UserEdit'
import Trainings from '../../Trainings/container/Trainings'
import UserSubscriptions from '../container/UerSubscriptions'
import UserTable from './UserTable'
import SubscriptionOrder from '../../Order/container/SubscriptionOrder'
import ProductOrder from '../../Order/container/ProductOrder'
import ProductsList from '../../Products/container/ProductsList'
import UpdateBal from '../container/UpdateBal'
import PersonalTrainingsOrder from '../../Order/container/PersonalTrainingsOrder'
import {
  Route,
  Switch,
  useRouteMatch,
  Link
}
  from "react-router-dom"
import { userInfo } from 'os';

function SwitchPanel(props) {
  const {
    uuid,
    users,
    setCurrentUser,
    actionButtons,
    displayLinesAmount
  } = props
  let { path, url } = useRouteMatch();
  return (
      <Switch>
        <Route exact path={path}>
          <UserTable
            users={users}
            setCurrentUser={setCurrentUser}
            displayLinesAmount={displayLinesAmount}
            actionButtons={actionButtons}
          />
        </Route>
        <Route exact path={`${path}/userinfo/subscriptionorder`} component={SubscriptionOrder}/>
        <Route exact path={`${path}/userinfo`}>
          <UserInfo uuid={props.uuid} />
        </Route>
        <Route exact path={`${path}/userinfo/useredit`}>
          <UserEdit />
        </Route>
        <Route exact path={`${path}/userinfo/trainings`}>
          <Trainings />
        </Route>
        <Route exact path={`${path}/userinfo/subscriptions`}>
          <UserSubscriptions />
        </Route>
        <Route exact path={`${path}/userinfo/product`}>
          <ProductsList changeButtonColumnCaption='Обрати товар' changeAction={'By'}/>
        </Route>        
        <Route exact path={`${path}/userinfo/productorder`}>
          <ProductOrder from={'userinfo'} />
        </Route>  
        <Route exact path={`${path}/userinfo/updatebal`}>
          <UpdateBal />
        </Route>
        <Route exact path={`${path}/userinfo/trainingorder`}>
          <PersonalTrainingsOrder />
        </Route>
      </Switch>
  )
}
export default SwitchPanel