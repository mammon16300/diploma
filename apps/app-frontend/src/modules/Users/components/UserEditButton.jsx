import React from 'react'
import qs from 'querystring'
import {
    useRouteMatch,
    Link} from "react-router-dom"

function UserEditButton(props) {
    const {row} = props
    let { url } = useRouteMatch();
    return (
    <Link 
    to={ `${url}/userinfo`}
    onClick={() => props.setCurrentUser(row.uuid)}
    >Перейти</Link>
    )
}
export default UserEditButton