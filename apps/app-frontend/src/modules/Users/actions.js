import { types, NAME } from './reducer'
import { RSAA } from 'redux-api-middleware'
import Cookies from 'universal-cookie'
import qs from 'querystring'
import {actions as adminActions } from '../AdminMenu/actions'

const cookies = new Cookies()

const host = process.env.HOST || 'app-gw'

const entryPoint = `http://${host}/api`


export const actions = {
    getUsers: () => ({
        [RSAA]: {
            types: [
                {
                    type: types.GET_USERS_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.GET_USERS_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            return data.users
                        })

                },
                {
                    type: types.GET_USERS_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/users`,
            credentials: 'include',
            method: 'GET'
        }
    }),
    updateUserByUuid: (user) => (dispatch, getState) => {
        const { users } = getState()[NAME]
        users.user = [...users.user,user]
        return dispatch({
          type: types.UPDATE_USER_BY_UUID,
          payload: users
        })
      },
    setCurrentUser: (value) => ({
        type: types.SELECTED_USER_SET, 
        payload: value
    }),
    getUser: (uuid) => ({
        [RSAA]: {
            types: [
                {
                    type: types.REQUESTED_USER_INFO_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.REQUESTED_USER_INFO_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            return data.user
                        })
                },
                {
                    type: types.REQUESTED_USER_INFO_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/user?${qs.stringify({uuid})}`,
            credentials: 'include',
            method: 'GET'
        }
    }),
    getUserSubscriptions: (requestData) => ({
        [RSAA]: {
            types: [
                {
                    type: types.REQUESTED_USER_SUBSCRIPTIONS_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.REQUESTED_USER_SUBSCRIPTIONS_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                       
                            return data
                        })
                },
                {
                    type: types.REQUESTED_USER_SUBSCRIPTIONS_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/userSubscriptions?${qs.stringify(requestData)}`,
            credentials: 'include',
            method: 'GET'
        }
    }),
    pauseUserSubscription: (uuid,userId) => dispatch => dispatch({ 
        [RSAA]: {
            types: [
                {
                    type: types.PAUSE_USER_SUBSCRIPTION_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.PAUSE_USER_SUBSCRIPTION_SUCCESS,
                    payload: (action, state, res) => {
                        dispatch(actions.getUserSubscriptions({userId: userId}))
                        return
                    }

                },
                {
                    type: types.PAUSE_USER_SUBSCRIPTION_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/pauseUserSubscription/${uuid}`,
            credentials: 'include',
            method: 'PATCH'
        }
    }),
    resumeUserSubscription: (uuid,userId) => dispatch => dispatch({ 
        [RSAA]: {
            types: [
                {
                    type: types.RESUME_USER_SUBSCRIPTION_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.RESUME_USER_SUBSCRIPTION_SUCCESS,
                    payload: (action, state, res) => {
                        dispatch(actions.getUserSubscriptions({userId: userId}))
                        return
                    }

                },
                {
                    type: types.RESUME_USER_SUBSCRIPTION_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/resumeUserSubscription/${uuid}`,
            credentials: 'include',
            method: 'PATCH'
        }
    }),
    registerUser: (data) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.REGISTRATION_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.REGISTRATION_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({ data }) => {
                            dispatch(actions.setCurrentUser(data.user.uuid))
                            dispatch(adminActions.redirectTo('/user/list/userinfo'))
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Користувач успішно зареєстрований!'
                            }))                    
                            return
                        })

                },
                {
                    type: types.REGISTRATION_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі зареєструвати користувача!'
                        }))
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/user`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    getCoaches: () => (dispatch, getState) => {
        return getState()[NAME].users.filter(item => item.role === 'coach')
    },
    getCoachClients: (coachId) => (dispatch, getState) => {
        return getState()[NAME].users.filter(item => item.defaultCoachId === coachId)
    },
    findUser: (uuid) =>(dispatch, getState) => {
        return getState()[NAME].users.find(item=> item.uuid === uuid)
    },
    editUser: (uuid, userData) => dispatch => dispatch({ 
        [RSAA]: {
            types: [
                {
                    type: types.EDIT_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.EDIT_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Данні користувача змінено!'
                            }))  
                            dispatch(adminActions.redirectTo('/user/list/userinfo'))
                            dispatch(actions.getUsers('user'))   
                            return 
                        })

                },
                {
                    type: types.EDIT_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі змінити данні користувача!'
                        }))
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/user/${uuid}`,
            credentials: 'include',
            method: 'PATCH',
            body: JSON.stringify(userData),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    updateOwnProfile: (data) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.REGISTRATION_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.REGISTRATION_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({ data }) => {
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Пароль успішно змінено!'
                            }))                    
                            return
                        })

                },
                {
                    type: types.REGISTRATION_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі змінити пароль!'
                        }))
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/updateOwnProfile`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    updateBal: (data) => dispatch => dispatch({ 
        [RSAA]: {
            types: [
                {
                    type: types.UPDATE_BAL_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.UPDATE_BAL_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Баланс поповнено!'
                            }))  
                            dispatch(adminActions.redirectTo('/user/list/userinfo'))
                            dispatch(actions.getUsers('user'))   
                            return 
                        })

                },
                {
                    type: types.UPDATE_BAL_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі поповнити баланс користувача!'
                        }))
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/balance/${data.userId}`,
            credentials: 'include',
            method: 'PATCH',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        }
    })
}