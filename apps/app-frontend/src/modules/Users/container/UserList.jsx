import React from 'react';
import { connect, } from 'react-redux'
import { NAME as USERS_NAME } from '../reducer'
import { bindActionCreators } from 'redux'
import { actions as userListActions } from '../actions'
import SwichPanel from '../components/SwichPanel'

let ReactDOM = require('react-dom');

function mapStateToProps(state) {
  return {
    ...state[USERS_NAME]
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...userListActions
}, dispatch)

class UserList extends React.Component {
  constructor(props) {
    super(props);
    props.getUsers() 
  }
  render() {
    const {
      users,
      displayLinesAmount,
      actionButtons
    } = this.props
    return (
        <SwichPanel
          uuid={this.props.selectedUserId}
          users={users}
          displayLinesAmount={displayLinesAmount}
          setCurrentUser={this.props.setCurrentUser}
          actionButtons={actionButtons}
        />
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserList)
