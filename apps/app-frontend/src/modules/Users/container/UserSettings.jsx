import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'
import TextInput from '../../../components/TextInput'
import { NAME as USERS_NAME } from '../reducer'
import { actions as usersActions } from '../actions'
import AvatarEditor from '../../../components/AvatarEditor'
import {Button} from 'react-bootstrap'

function mapStateToProps(state) {
  return {
    currentUser: state[USERS_NAME].user
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...usersActions
}, dispatch)


class UsersSettings extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      amount: 0
    }
  }


  submitForm = () => {
    this.props.updateOwnProfile({oldPassword: this.state.oldPassword,newPassword: this.state.newPassword})   
  }

  render() {
    const user = this.props.currentUser
    return (
      <div className="container emp-content">
        <h3 className="well">Зміна паролю</h3>
        <div className='row'>
          <div className="col well">
              <form>
                  <TextInput
                    //defaultValue={user.cardNumber}
                    label='Ваш пароль'
                    type="password"
                    placeholder="Введіть ваш пароль.."
                    onChangeHandler={e => this.setState({ oldPassword: e.target.value })}
                  /> 
                  <TextInput
                    //defaultValue={user.cardNumber}
                    label='Новий пароль'
                    type="password"
                    placeholder="Введіть новий пароль.."
                    onChangeHandler={e => this.setState({ newPassword: e.target.value })}
                  /> 
                  <TextInput
                    //defaultValue={user.cardNumber}
                    label='Повторіть новий пароль'
                    type="password"
                    placeholder="Введіть новий пароль ще раз.."
                    onChangeHandler={e => this.setState({ repeatPassword: e.target.value })}
                  /> 
              </form>
          </div>
        </div>
        <Button className='form-submit-btn' variant='primary' onClick={() => { this.submitForm() }} >Змінити пароль</Button>  
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UsersSettings)
