import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { NAME as USER_EDIT_NAME } from '../reducer'
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'
import TextInput from '../../../components/TextInput'
import { NAME as USERS_NAME } from '../../Users/reducer'
import { actions as userEditActions } from '../actions'
import AvatarEditor from '../../../components/AvatarEditor'
import Select from 'react-select'
import { Button } from 'react-bootstrap'

function mapStateToProps(state) {
  return {
    currentUser: state[USERS_NAME].user
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...userEditActions
}, dispatch)


class UserEdit extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isImageEdited: false
    }
  }

  setEditorRef = (editor) => { this.editor = editor }  //referens to component for image edit

  submitForm = () => {
    let photoBase64
    if (this.editor) {
      photoBase64 = this.editor.getImageScaledToCanvas().toDataURL()
    }

    const data = {
      ...this.state,
      photoBase64
    }
    if (this.state.defaultCoachId)
      data.defaultCoachId = this.state.defaultCoachId
    if (!this.state.isImageEdited) delete data.photoBase64
    delete data.isImageEdited

    this.props.editUser(this.props.currentUser.uuid, data)
  }

  onImageChange = (value) => {
    this.setState({ isImageEdited: value })
  }

  render() {
    const user = this.props.currentUser
    //----------------------
    const coachOptions = []
    const coaches = this.props.getCoaches()
    coaches.map((coach, key) => {
      coachOptions.push({
        value: coach.uuid,
        label: coach.firstName + ' ' + coach.lastName
      })
    })
    //-----------------------------------
    return (
      <div className="container emp-content">
        <h3 className="well">Редагування користувача</h3>
        <div className='row'>

          <div className='col well avatar-container'>
            <AvatarEditor
              setEditorRef={this.setEditorRef}
              imageUrl={user.photoUrl}
              toggleHandler={this.onImageChange}
            />
          </div>

          <div className="col well">
            <form>
              <div className="row">
                <TextInput
                  defaultValue={user.firstName}
                  isRow
                  label='Имя'
                  type="text"
                  placeholder="Введите имя сюда.."
                  onChangeHandler={e => this.setState({ firstName: e.target.value })}
                />
                <TextInput
                  defaultValue={user.lastName}
                  isRow
                  label='Фамилия'
                  type="text"
                  placeholder="Введите фамилию сюда.."
                  onChangeHandler={e => this.setState({ lastName: e.target.value })}
                />
              </div>
              <TextInput
                defaultValue={user.cardNumber}
                label='Карта в спорт зале'
                type="text"
                placeholder="Введите номер карты в зале.."
                onChangeHandler={e => this.setState({ cardNumber: e.target.value })}
              />
              <div className="row">
                <TextInput
                  defaultValue={user.phoneNumber}
                  isRow
                  label='Телефон'
                  type="text"
                  placeholder="Введите телефон сюда.."
                  onChangeHandler={e => this.setState({ phoneNumber: e.target.value })}
                />
                <TextInput
                  defaultValue={user.email}
                  isRow
                  label='E-mail'
                  type="text"
                  placeholder="Електронный адрес.."
                  onChangeHandler={e => this.setState({ email: e.target.value })}
                />
              </div>
              <div className='form-group'>
                <label>Тренер користувача:</label>
                <Select options={coachOptions}
                  defaultValue={user.coach ? { value: user.coach.uuid, label: user.coach.firstName + ' ' + user.coach.lastName } : ''}
                  onChange={e => {
                    this.setState({ defaultCoachId: e.value })
                  }} />
              </div>
              <div className='form-group'>
                <label>Стать:</label>
                <div className="row">
                  <div className="col form-group">
                    <label className="radio inline">
                      <input
                        name="gender" type="radio"
                        defaultChecked={user.gender == 'male' ? true : false}
                        onChange={() => this.setState({ gender: 'male' })}
                      />
                      <span style={{ marginRight: '30px' }}> Чоловік</span>
                    </label>
                    <label className="radio inline">
                      <input
                        name="gender" type="radio"
                        defaultChecked={user.gender == 'female' ? true : false}
                        onChange={() => this.setState({ gender: 'female' })}
                      />
                      <span> Жінка</span>
                    </label>
                  </div>
                </div>
              </div>
              <Button className='form-submit-btn' variant='primary' onClick={() => { this.submitForm() }} >Підтвердити зміни</Button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserEdit)
