import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as orderActions } from '../actions'
import { NAME as ORDER_NAME } from '../reducer'
import { NAME as USERS_NAME} from '../../Users/reducer'
import { NAME as ROOT_NAME } from '../../Root/reducer'
import InfoRow from '../../../components/InfoRow'
import {Button} from 'react-bootstrap'
import Select from 'react-select'
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'

import TextInput from '../../../components/TextInput'

function mapStateToProps(state) {
    return {
        ...state[ORDER_NAME],
        userRole: state[ROOT_NAME].user.role,
        uuid: state[USERS_NAME].selectedUserId,
        userName: state[USERS_NAME].user.firstName,        
        userBal: state[USERS_NAME].user.balance?state[USERS_NAME].user.balance:state[ROOT_NAME].user.balance,
        
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...orderActions
}, dispatch)

class PersonalTrainingsOrder extends React.Component {
    constructor(props) {
        super(props)
        props.getServices()
        //console.log(props)
        this.state = {
            total: 0,
            amount: 0,
            isCash: true
        }
    }
    submitForm = () => {
        let data ={}
        if(['manager', 'admin'].includes(this.props.userRole))
        data = {
            userId: this.props.uuid,
            amount: +this.state.amount,
            isCash: this.state.isCash
        }
        else
        data = {
            amount: +this.state.amount
        }
        console.log('data: ', data)
        this.props.purchasePersonalTrainings(data,this.props.userRole)
    }
    render() {
        return (
            <div className="container emp-content">
                <h3 className="well">Покупка особистих тренувань для {this.props.userName}</h3>
                    <div className="row">
                        <form className="col-sm-12">
                            <div className="col-sm-12">
                                <TextInput
                                    label='Кількість'
                                    type="number"
                                    placeholder="Введіть кількість сюди"
                                    onChangeHandler={e => this.setState({ amount: e.target.value, total: e.target.value*this.props.services.price })}
                                />                                
                                {['manager', 'admin'].includes(this.props.userRole) && <TextInput
                                    label='Розрахунок готівкою'
                                    type="checkbox"
                                    placeholder="Введіть кількість сюди"
                                    onChangeHandler={e => { this.setState({ isCash: e.target.checked }) }}
                                />}
                                <div className='form-group order-info-row'>
                                   <InfoRow
                                        name="Баланс користувача"
                                        value={this.props.userBal+' грн'}
                                    />
                                    <InfoRow
                                        name="Ціна за одиницю"
                                        value={this.props.services.price+' грн'}
                                    />
                                    <InfoRow
                                        name="Сума"
                                        value={this.state.total+' грн'}
                                    />
                                </div>
                            </div>
                        </form>
                </div>
                <Button className='form-submit-btn' variant='primary' onClick={() => { this.submitForm() }} >Оформити</Button> 
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PersonalTrainingsOrder)
