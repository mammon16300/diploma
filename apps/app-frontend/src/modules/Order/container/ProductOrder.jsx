import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as orderActions } from '../actions'
import { NAME as ORDER_NAME } from '../reducer'
import { NAME as USERS_NAME} from '../../Users/reducer'
import { NAME as PRODUCT_NAME} from '../../Products/reducer'
import { Button } from 'react-bootstrap'
import InfoRow from '../../../components/InfoRow'
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'

import TextInput from '../../../components/TextInput'

function mapStateToProps(state) {
    return {
        ...state[ORDER_NAME],
        userUuid: state[USERS_NAME].selectedUserId,
        userName: state[USERS_NAME].user.firstName,
        userBal: state[USERS_NAME].user.balance,
        productUuid: state[PRODUCT_NAME].selectedProductId,
        product: state[PRODUCT_NAME].product
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...orderActions
}, dispatch)

class ProductOrder extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          total: 0,
          from: props.from ? props.from : false,
          subscriptionId: '',
          amount: '',
          isCash: true
        }
    }
    submitForm = () => {
      let data
      
      if(this.state.from === 'userinfo')
      data = {
        userId: this.props.userUuid,
        productId: this.props.productUuid, 
        amount: this.state.amount, 
        isCash: this.state.isCash
      }
      else
      data = {
        productId: this.props.productUuid, 
        amount: this.state.amount, 
        isCash: this.state.isCash
      }
      console.log('from: ', data)
        this.props.purchaseProduct(data)
    }
    render() {
        console.log('order from user: ', this.state.from)
        return (
            <div className="container emp-content">
                <h3 className="well">Купівля {this.props.product.name} {this.state.from === 'userinfo' && 'для '+this.props.userName}</h3>
                    <div className="row">
                    <div className="col avatar-container">
                            <div className="profile-img">
                                <img className='profileImg' src={this.props.product.photoUrl} alt="" />
                            </div>
                        </div> 
                        <form className="col">
                            <div className="col-sm-12">
                                <TextInput
                                    label='Кількість'
                                    type="number"
                                    placeholder="Введіть кількість сюди"
                                    onChangeHandler={e => {
                                      this.setState({ amount: +e.target.value, total: e.target.value*this.props.product.price})
                                    }}
                                />   
                                {this.state.from === 'userinfo' &&                             
                                <TextInput
                                    label='Розрахунок готівкою'
                                    type="checkbox"
                                    placeholder="Введіть кількість сюди"
                                    onChangeHandler={e => { this.setState({ isCash: e.target.checked }) }}
                                />}
                                <div className='form-group order-info-row'>
                                    <InfoRow
                                        name="Кількість на складі"
                                        value={this.props.product.amount}
                                    />
                                    {this.state.from === 'userinfo' &&
                                   <InfoRow
                                        name="Баланс користувача"
                                        value={this.props.userBal+' грн'}
                                    />}
                                    <InfoRow
                                        name="Ціна за одиницю"
                                        value={this.props.product.price+' грн'}
                                    />
                                    <InfoRow
                                        name="Сума"
                                        value={this.state.total+' грн'}
                                    />
                                </div>
                            </div>
                        </form>
                    </div>                    
                  <Button className='form-submit-btn' variant='primary' onClick={() => { this.submitForm() }} >Купити</Button> 
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductOrder)
