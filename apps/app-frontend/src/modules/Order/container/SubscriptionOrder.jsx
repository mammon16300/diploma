import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as orderActions } from '../actions'
import { NAME as ORDER_NAME } from '../reducer'
import { NAME as USERS_NAME} from '../../Users/reducer'
import { NAME as ROOT_NAME } from '../../Root/reducer'
import InfoRow from '../../../components/InfoRow'
import {Button} from 'react-bootstrap'
import Select from 'react-select'
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'

import TextInput from '../../../components/TextInput'

function mapStateToProps(state) {
    return {
        ...state[ORDER_NAME],
        userRole: state[ROOT_NAME].user.role,
        uuid: state[USERS_NAME].selectedUserId,
        userName: state[USERS_NAME].user.firstName,        
        userBal: state[USERS_NAME].user.balance?state[USERS_NAME].user.balance:state[ROOT_NAME].user.balance,
        
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...orderActions
}, dispatch)

class SubscriptionOrder extends React.Component {
    constructor(props) {
        super(props)
        props.getSubscriptions()
        console.log(props)
        this.state = {
            subsctiptionPrice: 0,
            total: 0,
            subscriptionId: '',
            amount: 0,
            isCash: true
        }
    }
    submitForm = () => {
        let data ={}
        if(['manager', 'admin'].includes(this.props.userRole))
        data = {
            userId: this.props.uuid,
            productId: this.state.subscriptionId,
            amount: this.state.amount,
            isCash: this.state.isCash
        }
        else
        data = {
            productId: this.state.subscriptionId,
            amount: this.state.amount
        }
        
        this.props.purchaseSubscription(data, this.props.userRole)
    }
    render() {
        const options = []
        if (this.props.subscriptions) {
            this.props.subscriptions.map((subscription, key) => {
                options.push({
                    value: subscription.uuid,
                    label: subscription.name,
                    price: subscription.price
                })
            })
        }
        return (
            <div className="container emp-content">
                <h3 className="well">Оформлення Абонементу для {this.props.userName}</h3>
                    <div className="row">
                        <form className="col-sm-12">
                            <div className="col-sm-12">
                            <div className='form-group'>
                                    <label>Тип абонементу:</label>
                                    <Select options={options}
                                        onChange={e => {
                                            this.setState({ subscriptionId: e.value, subsctiptionPrice: e.price,  total: this.state.amount*e.price })
                                        }} />
                                </div>
                                <TextInput
                                    label='Кількість'
                                    type="number"
                                    placeholder="Введіть кількість сюди"
                                    onChangeHandler={e => this.setState({ amount: e.target.value, total: e.target.value*this.state.subsctiptionPrice })}
                                />                                
                                {['manager', 'admin'].includes(this.props.userRole) && <TextInput
                                    label='Розрахунок готівкою'
                                    type="checkbox"
                                    placeholder="Введіть кількість сюди"
                                    onChangeHandler={e => { this.setState({ isCash: e.target.checked }) }}
                                />}
                                <div className='form-group order-info-row'>
                                   <InfoRow
                                        name="Баланс користувача"
                                        value={this.props.userBal+' грн'}
                                    />
                                    <InfoRow
                                        name="Ціна за одиницю"
                                        value={this.state.subsctiptionPrice+' грн'}
                                    />
                                    <InfoRow
                                        name="Сума"
                                        value={this.state.total+' грн'}
                                    />
                                </div>
                            </div>
                        </form>
                </div>
                <Button className='form-submit-btn' variant='primary' onClick={() => { this.submitForm() }} >Оформити</Button> 
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionOrder)
