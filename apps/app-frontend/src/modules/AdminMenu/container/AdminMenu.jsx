import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as adminMenuActions } from '../actions'
import { NAME as ADMIN_MENU } from '../reducer'
import { NAME as ROOT_NAME} from '../../Root/reducer'
import Sidebar from '../components/Sidebar'
import {CustomAlert, showAlert} from '../../../components/CustomAlert'
import {CustomRedirection} from '../../../components/CustomRedirection'
import Topbar from '../components/Topbar'
import {Collapse} from 'react-bootstrap'
import { Route, Switch } from "react-router-dom";
import UserSettings from '../../Users/container/UserSettings'

import routes from '../../../routes'

function mapStateToProps(state) {
  return {
    ...state[ADMIN_MENU],
    user: state[ROOT_NAME].user
  }
}


const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...adminMenuActions
}, dispatch)


class AdminMenu extends React.Component {
  constructor(props) {
    super(props)
    this.AdminMenu = React.createRef()    
    this.state = {
      calssIsActive: ''
    }
  }
  render() {
    const {alert} = this.props
    return (
      <div className="wrapper d-flex align-items-stretch" id="content">
        <Sidebar
          {...this.props}
          routes={routes}
          classIsActive={this.state.classIsActive}
        />
        <div className='w-100'>
        <Topbar location={this.props.history.location.pathname} classIsActive={this.state.classIsActive} sideBarAction={()=>{
            if(this.state.classIsActive === 'active')
            this.setState({classIsActive: ''})
            else
            this.setState({classIsActive: 'active'})
          }}/>
        <div className="p-4 p-md-5">        
          
          <Collapse in={this.props.alertIsShow}>
          <div>
         <CustomAlert alertIsShow={this.props.alertIsShow} alert={alert} close={this.props.resetAlert}/>
          </div>
          </Collapse>
          <Switch>
            {this.props.redirect && <CustomRedirection redirectTo={this.props.redirect} reset={this.props.resetRedirect}/>}
            {routes.map((route, index) => {              
              if (route.childs) {
                if(route.permisionsFor.includes(this.props.user.role))
                return (                  
                  route.childs.map((childRoute, childIndex) => {
                    if(childRoute.permisionsFor.includes(this.props.user.role))
                    return (
                      <Route
                        path={childRoute.layout + childRoute.path}
                        component={childRoute.component}
                        key={childIndex}
                      />
                    )
                  })
                );
              }
              else
              { 
                if(route.permisionsFor.includes(this.props.user.role))
                return (
                  <Route
                    path={route.layout + route.path}
                    exact={route.exact}
                    component={route.component}
                    key={index}
                  />
                );
              }
            })}
            <Route path='/user/settings'
            exact='exact'
            component={UserSettings}
            key='settings' />
          </Switch>
        </div>
        </div>        
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AdminMenu)
