import React from "react";
import { NavLink } from "react-router-dom";
import topBarRoutes from './topBarRoutes'

class Tobbar extends React.Component {
  constructor(props) {
    super(props);
    this.Tobbar = React.createRef();
  }
  renderNavLinks = () => {
    let locationArray = this.props.location.split('/')
    locationArray.shift()    
    let currentInMap = topBarRoutes    
    return locationArray.map((pathCaption, index) => {
      let canBeNext = currentInMap.find(location => location.pathName == pathCaption)      
      if (canBeNext)
        if (canBeNext.child) {
          currentInMap = canBeNext.child 
          if(index === locationArray.length-1)
          {
            if (canBeNext.links) {
              return canBeNext.links.map((item, index) => {
                return (
                  <li
                    className=''
                    key={index}
                  >
                    <NavLink
                      to={item.to}
                      className={item.className}
                    >
                      {item.caption}
                    </NavLink>
                  </li>
                )
              })
            }
          }         
        }
        else {
          if (canBeNext.links) {
            return canBeNext.links.map((item, index) => {
              return (
                <li
                  className='nav-item'
                  key={index}
                >
                  <NavLink
                    to={item.to}
                    className={item.className}
                  >
                    {item.caption}
                  </NavLink>
                </li>
              )
            })
          }
        }
    })
  }
  render() {
    return (
      <nav className={this.props.classIsActive?'sidebar-hiden navbar navbar-expand-lg navbar-light bg-light' : 'navbar navbar-expand-lg navbar-light bg-light'}>
        <div className="container-fluid">

          <button onClick={this.props.sideBarAction} type="button" id="sidebarCollapse" className="btn btn-primary">
            <i className="fa fa-bars"></i>
            <span className="sr-only">Toggle Menu</span>
          </button>
          <button className="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i className="fa fa-bars"></i>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="nav navbar-nav ml-auto">
              {this.renderNavLinks()}
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}
export default Tobbar;