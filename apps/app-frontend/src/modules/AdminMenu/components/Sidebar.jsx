import React from "react";
import { NavLink } from "react-router-dom";
import Cookies from 'universal-cookie'

const cookies = new Cookies()

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.Sidebar = React.createRef();
  }
  renderNavLinks = () => {
    let pathArray = this.props.location.pathname.split('/')
    pathArray[0] = this.props.location.pathname;
    let isActiveClass = ''
    return this.props.routes.map((route, index) => {
      if (route.childs) {
        if (route.permisionsFor.includes(this.props.user.role)) {
          if(pathArray[1] == route.path.slice(1))
          isActiveClass = 'active'
          else
          isActiveClass = ''
          return (
            <li
              className={isActiveClass}
              key={index}
            >
              <a className="dropdown-toggle" aria-expanded="false" href={'#' + route.name + 'Submenu'} data-toggle="collapse">{route.name}</a>
              <ul className="collapse list-unstyled" id={route.name + 'Submenu'}>
                {
                  route.childs.map((childRoute, childIndex) => {
                    if (childRoute.permisionsFor.includes(this.props.user.role))
                    {
                      if(pathArray[2] == childRoute.path.slice(1) && pathArray[1] == route.path.slice(1))
                        isActiveClass = 'active'
                        else
                        isActiveClass = ''
                      return (
                        <li
                          className={isActiveClass}
                          key={childIndex}
                        >
                          <NavLink
                            to={childRoute.layout + childRoute.path}
                            className="nav-link"
                          >
                            <i className={childRoute.icon} />
                            {childRoute.name}
                          </NavLink>
                        </li>
                      )
                    }
                  })
                }
              </ul>
            </li>
          );
        }
      }
      else {
        if (route.permisionsFor.includes(this.props.user.role))
        {
          if(pathArray[1] == route.path.slice(1))
          isActiveClass = 'active'
          else
          isActiveClass = ''
          return (
            <li
              className={isActiveClass}
              key={index}
            >
              <NavLink
                to={route.layout + route.path}
                className="nav-link"
              >
                <i className={route.icon} />
                {route.name}
              </NavLink>
            </li>
          );
        }
      }
    })
  }
  render() {
    return (
      <nav id="sidebar" className={this.props.classIsActive}>
        <div className="p-4 pt-5">
          <a className="img logo mb-5" style={{ backgroundImage: 'url(/assets/images/logo.png)' }} href="#" />
          <ul className="list-unstyled components mb-5">
            {this.renderNavLinks()}
          </ul>
          
          <div className="footer">
          <NavLink
                to='/user/settings'
              >
                Change pussword
              </NavLink>
              {'  |  '}
            <a 
            href='/'
            onClick={()=>{
              cookies.remove('token',{path: '/'})
              cookies.remove('token',{path: '/',domain: 'app-gw'}) //do not work
            }}>Logout</a>
            <p>
              Copyright © Sviatoslav and Vova <br/> Develop studio
            </p>
          </div>
        </div>
      </nav>
    );
  }
}


export default Sidebar;