import {
  useRouteMatch,
  Link
} from "react-router-dom"
import qs from 'querystring'

const topBarRoutes = [
  {
    pathName: 'gymsessions',
    links: [
      {
        to: '/gymsessions',
        className: 'nav-link',
        caption: 'Список користувачів в сесії'
      },
      {
        to: '/gymsessions/registernew',
        className: 'nav-link',
        caption: 'Додати користувача в сесію'
      },
    ]
  },
  {
    pathName: 'user',
    child: [
      {
        pathName: 'list',
        child: [
          {
            pathName: 'userinfo',
            links: [    
              {
                to: `/user/list`,
                className: "nav-link",
                caption: 'Повернутися до списку'
              },
              {
                to: '/user/list/userinfo',
                className: 'nav-link',
                caption: 'Інформація користувача'
              },
              {
                to: `/user/list/userinfo/useredit`,
                className: 'nav-link',
                caption: 'Редагувати'
              },
              {
                to: `/user/list/userinfo/trainings`,
                className: "nav-link",
                caption: 'Графік тренувань'
              },
              {
                to: `/user/list/userinfo/subscriptions`,
                className: "nav-link",
                caption: 'Абонементи'
              },
              {
                to: `/user/list/userinfo/subscriptionorder`,
                className: "nav-link",
                caption: 'Оформити Абонемент'
              },
              {
                to: `/user/list/userinfo/product`,
                className: "nav-link",
                caption: 'Купити товар'
              },
              {
                to: `/user/list/userinfo/trainingorder`,
                className: "nav-link",
                caption: 'Купити особисте тренування'
              },
              {
                to: `/user/list/userinfo/updatebal`,
                className: "nav-link",
                caption: 'Поповнити баланс'
              }                                   
            ]
          }
        ]
      }
    ]
  },
  {
    pathName: 'products',
    child: [
      {
        pathName: 'list',
        child: [
          {
            pathName: 'productinfo',
            links: [
              {
                to: `/products/list`,
                className: "nav-link",
                caption: 'Повернутися до списку'
              },
              {
                to: '/products/list/productinfo',
                className: 'nav-link',
                caption: 'Сторінка товару'
              },
              {
                to: `/products/list/productinfo/edit`,
                className: 'nav-link',
                caption: 'Редагувати'
              },
              {
                to: `/products/list/productinfo/productorder`,
                className: "nav-link",
                caption: 'Купити товар'
              } 
            ]
          }
        ]
      }
    ]
  }
]
export default topBarRoutes;