import moment from 'moment'
export const NAME = 'TRAININGS'
export const types = {
    REQUESTED_USER_TRAININGS_REQUEST: `${NAME}/REQUESTED_USER_TRAININGS_REQUEST`,
    REQUESTED_USER_TRAININGS_SUCCESS: `${NAME}/REQUESTED_USER_TRAININGS_SUCCESS`,
    REQUESTED_USER_TRAININGS_FAILURE: `${NAME}/REQUESTED_USER_TRAININGS_FAILURE`,
    CREATE_TRAINING_REQUEST: `${NAME}/CREATE_TRAINING_REQUEST`,
    CREATE_TRAINING_SUCCESS: `${NAME}/CREATE_TRAINING_SUCCESS`,
    CREATE_TRAINING_FAILURE: `${NAME}/CREATE_TRAINING_FAILURE`,
    DECLINE_TRAINING_REQUEST: `${NAME}/DECLINE_TRAINING_REQUEST`,
    DECLINE_TRAINING_SUCCESS: `${NAME}/DECLINE_TRAINING_SUCCESS`,
    DECLINE_TRAINING_FAILURE: `${NAME}/DECLINE_TRAINING_FAILURE`
}

export const initialState = {
    events: []
}



export function reducer(state = initialState, action) {
    switch (action.type) {
        case types.REQUESTED_USER_TRAININGS_SUCCESS:
            return {
                ...state,
                events: action.payload.map((events, index) => {
                    return {
                        key: events.uuid,
                        coachId: events.coachId,
                        type: events.type,
                        userId: events.userId,
                        declinedBy: events.declinedBy,
                        start: moment(Date.parse(events.start)).toDate(),
                        end: moment(Date.parse(events.end)).toDate(),
                        title: events.type === 'duo'?events.fullName + ' з тренером':events.fullName,
                    }
                }
                )
            }
        case types.CREATE_TRAINING_SUCCESS:
                return {
                    ...state,
                    events: [
                        ...state.events,
                        {
                            key: action.payload.uuid,
                            coachId: action.payload.coachId,
                            type: action.payload.type,
                            userId: action.payload.userId,
                            declinedBy: action.payload.declinedBy,
                            start: moment(Date.parse(action.payload.start)).toDate(),
                            end: moment(Date.parse(action.payload.end)).toDate(),
                            title: action.payload.type === 'duo'?action.payload.fullName + ' з тренером':action.payload.fullName
                        }
                    ]
                }
        default:
            return { ...state }
    }
}