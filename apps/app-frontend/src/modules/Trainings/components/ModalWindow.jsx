import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
function ModalWindow(props) {
   // const [show, setShow] = React.useState(false);

    //const handleClose = () => setShow(false);
    //const handleShow = () => setShow(true);

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{props.text.title}</Modal.Title>
                </Modal.Header>
    <Modal.Body>{props.text.content}</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.handleClose}>
                        {props.text.cancel}
          </Button>
                    <Button variant="primary" onClick={props.submitAction}>
                        {props.text.submit}
          </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}
export default ModalWindow