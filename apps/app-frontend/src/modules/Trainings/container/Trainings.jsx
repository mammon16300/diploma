import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as trainingsActions } from '../actions'
import { actions as usersActions } from '../../Users/actions'
import { NAME as TRAININGS_NAME } from '../reducer'
import { NAME as USERS_NAME } from '../../Users/reducer'
import { NAME as ROOT_NAME } from '../../Root/reducer'
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'
import "react-big-calendar/lib/css/react-big-calendar.css"
import ModalWindow from '../components/ModalWindow'
import { Dropdown } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'
import TextInput from '../../../components/TextInput'
//calendar
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import 'moment/locale/uk'
//------------
import Modal from "react-bootstrap/Modal";
import CalendarToolBar from '../components/CalendarToolBar'
// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
// const localizer = momentLocalizer(moment)

let personalEvents = [];
function mapStateToProps(state) {
  return {
    ...state[TRAININGS_NAME],
    users: state[USERS_NAME].users,
    selectedUser: state[USERS_NAME].user,
    currentUser: state[ROOT_NAME].user,
    userRole: state[ROOT_NAME].user.role
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...trainingsActions,
  ...usersActions
}, dispatch)
class Trainings extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showPopup: false,
      fullCalendar: false,
      CoachClientId: props.selectedUser.uuid ? props.selectedUser.uuid : props.currentUser.uuid,
      showPersonal: true,
      modalText: {
        title: '',
        content: '',
        submit: '',
        cancel: ''
      },
      modalSubmitAction: undefined
    }
    this.getPersonalTrainings()
    if (['coach'].includes(props.userRole) || ['coach'].includes(props.selectedUser.role)) {
      ['coach'].includes(props.selectedUser.role) ?
        this.state.coachClients = props.getCoachClients(this.props.selectedUser.uuid)
        :
        this.state.coachClients = props.getCoachClients(this.props.currentUser.uuid)

      this.state.coachClientsOptions = [{
        isSelected: true,
        key: 'my',
        value: this.props.selectedUser.uuid ? this.props.selectedUser.uuid : this.props.currentUser.uuid,
        text: 'Особисті тренування',
        selected: true
      }]
      this.state.coachClients.map((client, key) => {
        this.state.coachClientsOptions.push({
          key: key,
          value: client.uuid,
          text: client.firstName + ' ' + client.lastName
        })
      })
    }
  }
  //-------------------------------------------
  getPersonalTrainings = () => {
    if(['manager', 'admin'].includes(this.props.userRole))
    {
      return this.getClientTrainings()
    }
    else
    return this.getClientTrainings(
      (data) => {
        personalEvents = data.map((events, index) => {
          return {
            key: events.uuid,
            coachId: events.coachId,
            type: events.type,
            userId: events.userId,
            start: moment(Date.parse(events.start)).toDate(),
            end: moment(Date.parse(events.end)).toDate(),
            title: events.type === 'duo'?events.fullName + ' з тренером':events.fullName,
            declinedBy: events.declinedBy
          }
        })
      })
  }
  //-------------------------------------
  getClientTrainings = async (savePersonal) => {
    let data = {}
    if (!savePersonal)
      if (this.state.fullCalendar) {
        data = { type: 'duo' }
      }
      else
        if (this.state.CoachClientId === this.props.currentUser.uuid || this.state.CoachClientId === this.props.selectedUser.uuid) {
          if (['manager', 'admin'].includes(this.props.userRole))
            data = {
              userId: this.props.selectedUser.uuid
            }
        }
        else {
          data = { userId: this.state.CoachClientId, type: 'duo' }
        }
    await this.props.getTrainings(data, savePersonal)
  }
  //--------------------------------------------------------------
  //-------------------------------------
  getFullCalendar = () => {
    if (this.state.fullCalendar) {

    }
  }
  //---------------------------------------------------------------------
  //-------------------------------------
  createTranningButton = () => {
    let data = {}
    if (this.state.CoachClientId === this.props.currentUser.uuid || this.state.CoachClientId === this.props.selectedUser.uuid) {
      if (['user', 'coach'].includes(this.props.userRole))
        data = {
          start: this.state.event.start,
          end: this.state.event.end,
          type: 'solo'
        }
      else
        data = {
          start: this.state.event.start,
          end: this.state.event.end,
          userId: this.props.selectedUser.uuid,
          type: 'solo'
        }
    }
    else {
      if (['coach'].includes(this.props.userRole))
        data = {
          userId: this.state.CoachClientId,
          start: this.state.event.start,
          end: this.state.event.end,
          type: 'duo'
        }
      else
        data = {
          userId: this.state.CoachClientId,
          coachId: this.props.selectedUser.uuid,
          start: this.state.event.start,
          end: this.state.event.end,
          type: 'duo'
        }
    }
    this.props.createTraining(data)
    this.setState({ showPopup: false })
  }
  //-----------------------------------------------------------------------
  //--------------------------------------------------
  eventStyleGetter = (event, start, end, isSelected) => {
    var backgroundColor = 'gray';
    if (event.userId === this.state.CoachClientId)
      backgroundColor = '#f0ad4e'
    if (event.declinedBy)
      backgroundColor = '#8a4c4c'
    var style = {
      border: 'none',
      boxSizing: 'border-box',
      boxShadow: 'none',
      margin: '0',
      padding: '2px 5px',
      backgroundColor: backgroundColor,
      bordeRadius: '5px',
      color: '#fff',
      cursor: 'pointer',
      width: '100 %',
      textAlign: 'left'
    };
    return {
      style: style
    };
  }
  //----------------------------------------------------------------
  declineTreining = (event) => {
    this.setState({
      event,
      modalText: {
        title: 'Видалити тренування?',
        content: event.start + event.end,
        submit: 'Видалити',
        cancel: 'Відміна'
      },
      modalSubmitAction: () => {
        this.props.declineTrainingEvent({ uuid: event.key }, this.refreshTrainings)
        this.setState({ showPopup: false })
      },
      showPopup: true
    })
  }
  createTraining = (event) => {
    this.setState({
      event,
      modalText: {
        title: 'Додати тренування?',
        content: event.start + event.end,
        submit: 'Додати тренування',
        cancel: 'Відміна'
      },
      modalSubmitAction: this.createTranningButton,
      showPopup: true
    })
  }
  //-------------------------------------
  refreshTrainings = () => {
    if (this.state.fullCalendar)
      this.getPersonalTrainings().then(() => {
        this.getFullCalendar()
        this.getClientTrainings()
      })
    else
      this.getClientTrainings()
  }
  //--------------------------------------------------------------------------
  render() {
    return (
      <div className="App container emp-content">
        <div>
          {this.state.coachClientsOptions &&
            <Dropdown
              onChange={(e, { value }) => {
                this.setState({ CoachClientId: value },
                  () => { this.getClientTrainings() }
                )
              }}
              placeholder='Оберіть клієнта'
              fluid
              search
              selection
              options={this.state.coachClientsOptions}
            />
          }
          {(['coach'].includes(this.props.userRole) || ['coach'].includes(this.props.selectedUser.role)) &&
            <TextInput
              label='Повний графік'
              type="checkbox"
              defaultValue={false}
              placeholder=""
              onChangeHandler={e => {
                const isChecked = e.target.checked
                this.getPersonalTrainings().then(() => {
                  this.setState({ fullCalendar: isChecked },
                    () => {
                      this.getFullCalendar()
                      this.getClientTrainings()
                    }
                  )
                })
              }}
            />
          }
        </div>
        <Calendar
          localizer={momentLocalizer(moment)}
          defaultDate={new Date()}
          defaultView="week"
          events={this.state.fullCalendar ? personalEvents.concat(this.props.events) : this.props.events}
          selectable={true}
          onSelectEvent={this.declineTreining}
          eventPropGetter={this.eventStyleGetter}
          onSelectSlot={this.createTraining}
          onSelecting={(range) => {
            return ((range.start - range.end) >= -3600000)
          }}
          components={{
            toolbar: CalendarToolBar
          }}
        />
        <ModalWindow
          handleClose={() => { this.setState({ showPopup: false }) }}
          show={this.state.showPopup}
          submitAction={this.state.modalSubmitAction}
          text={this.state.modalText} />
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Trainings)
