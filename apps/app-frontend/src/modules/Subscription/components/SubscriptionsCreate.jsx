import React, {useState} from 'react'
import qs from 'querystring'
import TextInput from '../../../components/TextInput'
import {Button} from 'react-bootstrap'
let ReactBsTable = require('react-bootstrap-table');


let BootstrapTable = ReactBsTable.BootstrapTable;
let TableHeaderColumn = ReactBsTable.TableHeaderColumn;

function SubscriptionList(props) {    
    const [name,setName] = useState('')
    const [description,setDescription] = useState('')
    const [price,setPrice] = useState('')    
    function submitForm() {
        props.createSubscription({
            name,
            description,
            price
        })
    
    }
    return (
        <div className="container emp-content">
                <h4 className="well">Створення абонементу</h4>
                    <div className="row">
                        <form className="col-sm-12">
                        
                            <div className="col-sm-12">                               
                                <TextInput
                                    label={`Назва`}
                                    type="text"
                                    placeholder="Назва абонементу"
                                    onChangeHandler={e => setName(e.target.value)}
                                />
                                    <TextInput
                                        label='Опис'
                                        type="text"
                                        placeholder="Опис абонементу"
                                        onChangeHandler={e => setDescription(e.target.value )}
                                    />
                                    <TextInput
                                        label='Ціна'
                                        type="number"
                                        placeholder="Ціна абонементу"
                                        onChangeHandler={e => setPrice(e.target.value)}
                                    />
                            </div>
                        </form>
                </div>
                <Button className='form-submit-btn' variant='primary' onClick={() => { submitForm() }} >Створити</Button> 
            </div>
    )
}
export default SubscriptionList