import React from 'react'
import qs from 'querystring'
import {
    useRouteMatch,
    Link
} from "react-router-dom"

let ReactBsTable = require('react-bootstrap-table');


let BootstrapTable = ReactBsTable.BootstrapTable;
let TableHeaderColumn = ReactBsTable.TableHeaderColumn;

function SubscriptionList(props) {
    let { url } = useRouteMatch();
    function buttonFormatter(cell, row){
        return <button
        onClick={() => props.deleteSubscription(row.uuid)}
        >Видалити</button>
    }
    const { subscriptions } = props
    return (
        <div className='emp-content'>
            <h4>Список абонементів</h4>
        <BootstrapTable
            data={subscriptions}
            striped hover
        >
            <TableHeaderColumn isKey dataField='uuid' hidden>Product ID</TableHeaderColumn>
            <TableHeaderColumn dataField='name'>Имя</TableHeaderColumn>
            <TableHeaderColumn dataField='description'>Опис</TableHeaderColumn>            
            <TableHeaderColumn dataField='price'>Ціна</TableHeaderColumn>
            <TableHeaderColumn dataField="button" dataFormat={buttonFormatter}>Видалити</TableHeaderColumn>
        </BootstrapTable>
        </div>
    )
}
export default SubscriptionList