export const NAME = 'ROOT'

export const types = {
  TEST_VAL_SET: 'TEST_VAL_SET',
  CURRENT_USER_INFO_REQUEST: `${NAME}/CURRENT_USER_INFO_REQUEST`,
  CURRENT_USER_INFO_SUCCESS: `${NAME}/CURRENT_USER_INFO_SUCCESS`,
  CURRENT_USER_INFO_FAILURE: `${NAME}/CURRENT_USER_INFO_FAILURE`
}

export const initialState = {
  testn: '24234234',
  user: {
    id: '',
    role: '',
    photoUrl: 'https://res.cloudinary.com/drcjrsq5t/image/upload/v1587587987/Portrait_Placeholder_scbpxj.png',
    cardNumber: '',
    email: '',
    firstName: '',
    lastName: '',
    phoneNumber: '',
    gender: '',
    password: '',
    uuid: '',
    signUpDate: '',
    __v: 0
  }
}



export function reducer(state = initialState, action) {
  switch (action.type) {
    case types.TEST_VAL_SET:
      return {
        ...state,
        testParam: action.payload
      }
    case types.CURRENT_USER_INFO_SUCCESS:
      return {
        ...state,
        user: action.payload
      }
    default:
      return { ...state }
  }
}