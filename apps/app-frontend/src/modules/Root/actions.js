import { types } from './reducer'
import { RSAA } from 'redux-api-middleware'
import qs from 'querystring'


const host = process.env.HOST || 'app-gw'

const entryPoint = `http://${host}/api`

export const actions = {
    setTestValue: (value) => ({type: types.TEST_VAL_SET, payload: value}),
    getCurrentUser: () => ({
        [RSAA]: {
            types: [
                {
                    type: types.CURRENT_USER_INFO_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.CURRENT_USER_INFO_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            return data.user
                        })
                },
                {
                    type: types.CURRENT_USER_INFO_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/currentUser`,
            credentials: 'include',
            method: 'GET'
        }
    })
}