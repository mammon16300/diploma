import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import moment from 'moment'
import 'moment/locale/uk'
import { Line } from 'react-chartjs-2'
import 'react-dates/initialize'
import { DateRangePicker } from 'react-dates'
import 'react-dates/lib/css/_datepicker.css'
import { Button } from 'react-bootstrap'
import qs from 'qs'

import { NAME, actions, entryPoint } from '../'

function mapStateToProps(state) {
  return {
    ...state[NAME]
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...actions
}, dispatch)


class AdminDashboard extends Component {
  constructor(props) {
    super(props)

    this.chartRef = React.createRef()

    this.state = {
      startDate: moment().startOf('month'),
      endDate: moment().endOf('day'),
    }
  }

  componentDidMount() {
    this.getData()
    this.setState({
      labels: this.getLabels()
    })
  }

  getData = () => {
    this.props.getRevenue({
      date: {
        from: this.state.startDate.toDate(),
        to: this.state.endDate.toDate()
      }
    })
    this.props.getVisits({
      date: {
        from: this.state.startDate.toDate(),
        to: this.state.endDate.toDate()
      }
    })
    this.setState({
      labels: this.getLabels()
    })
  }

  getLabels = () => {
    const startDate = this.state.startDate.clone()
    const endDate = this.state.endDate.clone()

    const dates = []
    let cursor
    const dateNow = moment().endOf('day')

    do {
      dates.push(startDate.clone().format('D MMM'))
      startDate.add(1, 'days')
    } while (startDate.diff(endDate) < 0)

    return dates
  }

  downloadRevnueReport = () => {
    const data = {
      date: {
        from: this.state.startDate.toDate(),
        to: this.state.endDate.toDate()
      }
    }
    window.location.assign(`${entryPoint}/revenueReport?${qs.stringify(data)}`)
  }

  render() {
    const {
      revenue,
      visits
    } = this.props

    return (
      <div className='container emp-content'>
        <DateRangePicker
          startDate={this.state.startDate}
          endDate={this.state.endDate}
          onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })}
          focusedInput={this.state.focusedInput}
          onFocusChange={focusedInput => this.setState({ focusedInput })}
          isOutsideRange={() => false}
          co
        />
        <Button onClick={this.getData}>Показати</Button>
        
        <Button onClick={this.downloadRevnueReport}>Завантажити звіт з доходів</Button>
        <div className=''>
          <h3>Дохід: </h3>
            <Line
              ref={this.chartRef}
              data={{
                labels: this.state.labels,
                datasets: [
                  {
                    borderColor: '#f8b739',
                    backgroundColor: 'rgba(248, 183, 57, 0.2)',

                    label: 'Дохід',
                    data: Object.values(revenue)
                  }
                ]
              }}
            />
          <h3>Відвідуваність: </h3>
            <Line
              ref={this.chartRef}
              data={{
                labels: this.state.labels,
                datasets: [
                  {
                    borderColor: '#f8b739',
                    backgroundColor: 'rgba(248, 183, 57, 0.2)',

                    label: 'Відвідування',
                    data: Object.values(visits)
                  }
                ]
              }}
            />
        </div>
      </div>
    )
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AdminDashboard)
