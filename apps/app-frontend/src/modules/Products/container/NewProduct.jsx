import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as productActions } from '../actions'
import { NAME as PRODUCT_NAME } from '../reducer'
import { Button } from 'react-bootstrap';
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'
import AvatarEditor from '../../../components/AvatarEditor'

import TextInput from '../../../components/TextInput'

function mapStateToProps(state) {
    return {
        ...state[PRODUCT_NAME]
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...productActions
}, dispatch)

class NewProduct extends React.Component {
    constructor() {
        super()
        this.state = {
            name: '',
            description: '',
            price: '',
            amount: ''
        }
    }
    setEditorRef = (editor) => { this.editor = editor }  //referens to component for image edit
    submitForm = () => {
        let photoBase64
        if (this.editor) {
            photoBase64 = this.editor.getImageScaledToCanvas().toDataURL()
        }
        this.props.newProduct(this.state.name, this.state.description, this.state.price, this.state.amount, photoBase64)
    }
    onImageChange = (value) => {
        this.setState({ isImageEdited: value })
    }
    render() {
        return (
            <div className="container emp-content">
                <h1 className="well">Додавання товару</h1>
                <div className="row">
                    <div className='col well avatar-container'>
                        <AvatarEditor
                            setEditorRef={this.setEditorRef}
                            imageUrl='https://res.cloudinary.com/drcjrsq5t/image/upload/v1591086428/img-default_gzuwoy.gif'
                            toggleHandler={this.onImageChange}
                        />
                    </div>
                    <div className="col well">
                        <div className="row">
                            <form className="col-sm-12">                          
                                    <TextInput
                                        label='Назва товару'
                                        type="text"
                                        placeholder="Введіть назву товару.."
                                        onChangeHandler={e => this.setState({ name: e.target.value })}
                                    />   
                                    <TextInput
                                        label='Опис товару'
                                        type="text"
                                        placeholder="Введіть опис товару.."
                                        onChangeHandler={e => this.setState({ description: e.target.value })}
                                    />  
                                    <TextInput
                                        label='Ціна'
                                        type="number"
                                        placeholder="Введіть ціну за одиницю товару.."
                                        onChangeHandler={e => this.setState({ price: e.target.value })}
                                    />  
                                    <TextInput
                                        label='Кількість'
                                        type="number"
                                        placeholder="Введіть кількість товару на складі.."
                                        onChangeHandler={e => this.setState({ amount: e.target.value })}
                                    />
                            </form>
                        </div>
                    </div>
                </div>
                <Button className='form-submit-btn' variant='primary' onClick={() => { this.submitForm() }} >Додати</Button>       
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewProduct)
