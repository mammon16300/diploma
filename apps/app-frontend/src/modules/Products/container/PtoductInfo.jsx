import React from 'react';
import { connect, } from 'react-redux'
import { NAME as PRODUCT_NAME } from '../reducer'
import { bindActionCreators } from 'redux'
import { actions as productActions } from '../actions'
import InfoRow from '../../../components/InfoRow'
import { ProductEditLink } from '../components/MatchLink'
import ModalWindow from '../components/ModalWindow'
import Button from 'react-bootstrap/Button';



function mapStateToProps(state) {
    return {
        ...state[PRODUCT_NAME]
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...productActions
}, dispatch)


class ProductInfo extends React.Component {
    constructor(props) {
        super(props)
        console.log('cc1: ',props)
        props.getProduct(props.selectedProductId)
        console.log('cc: ',props)
        this.state = {
            showPopup: false
        }
    }
    render() {
        const product = this.props.product
        return (
            <div>
                <ModalWindow 
                submitAction={()=>{                    
                    this.props.deleteProduct(this.props.selectedProductId)
                    this.setState({showPopup: false})
                }}
                handleClose={()=>{this.setState({showPopup: false})}} 
                show = {this.state.showPopup}
                name={this.props.product.name}
                description={this.props.product.description}
                />
                <div className='row'>
                    <div className="container emp-content col-md-8">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="profile-img">
                                    <img src={product.photoUrl} alt="" />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <Button onClick={()=>this.setState({showPopup: true})}>Видалити</Button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="user-info">
                                    <InfoRow
                                        name="Назва"
                                        value={product.name}
                                    />
                                    <InfoRow
                                        name="Опис товару"
                                        value={product.description}
                                    />
                                    <InfoRow
                                        name="Кількість на складі"
                                        value={product.amount}
                                    />
                                    <InfoRow
                                        name="Ціна"
                                        value={product.price}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductInfo)
