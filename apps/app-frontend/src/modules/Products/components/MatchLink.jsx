import React from 'react'
import qs from 'querystring'
import {
    useRouteMatch,
    Link} from "react-router-dom"

export function ProductEditLink() {
    let { url } = useRouteMatch();
    return (
        <Link 
        to={ `${url}/edit`}
        className="btn profile-edit-btn"
        >Редагувати</Link>
    )
}