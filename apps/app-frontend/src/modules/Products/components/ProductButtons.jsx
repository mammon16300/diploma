import React from 'react'
import qs from 'querystring'
import { Link } from "react-router-dom"

export function ProductEditButton(props) {
    const {row} = props
    return (
    <Link 
    to={ `/products/list/productinfo`}
    onClick={() => props.setCurrentProduct(row.uuid)}
    >Перейти</Link>
    )     
}
export function ProductByButton(props) {
    //Button for user list
    const {row} = props
    return (
    <Link 
    to={ `/user/list/userinfo/productorder`}
    onClick={() => {
        props.setCurrentProduct(row.uuid)
        props.getProduct(row.uuid)
    }}
    >Купити</Link>
    )     
}