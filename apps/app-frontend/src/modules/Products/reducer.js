export const NAME = 'PRODUCT'
export const types = {
    GET_PRODUCTS_REQUEST: `${NAME}/GET_PRODUCTS_REQUEST`,
    GET_PRODUCTS_SUCCESS: `${NAME}/GET_PRODUCTS_SUCCESS`,
    GET_PRODUCTS_FAILURE: `${NAME}/GET_PRODUCTS_FAILURE`,  
    NEW_PRODUCT_REQUEST: `${NAME}/NEW_PRODUCT_REQUEST`,
    NEW_PRODUCT_SUCCESS: `${NAME}/NEW_PRODUCT_SUCCESS`,
    NEW_PRODUCT_FAILURE: `${NAME}/NEW_PRODUCT_FAILURE`,
    SELECTED_PRODUCT_SET: `${NAME}/SELECTED_PRODUCT_SET`,
    FIND_PRODUCT: `${NAME}/GET_PRODUCT`,
    EDIT_REQUEST: `${NAME}/EDIT_REQUEST`,
    EDIT_SUCCESS: `${NAME}/EDIT_SUCCESS`,
    EDIT_FAILURE: `${NAME}/EDIT_FAILURE`,
    DELETE_REQUEST: `${NAME}DELETE_REQUEST`,
    DELETE_SUCCESS: `${NAME}DELETE_SUCCESS`,
    DELETE_FAILURE: `${NAME}DELETE_FAILURE`,
    GET_PRODUCT_REQUEST: `${NAME}/GET_PRODUCT_REQUEST`,
    GET_PRODUCT_SUCCESS: `${NAME}/GET_PRODUCT_SUCCESS`,
    GET_PRODUCT_FAILURE: `${NAME}/GET_PRODUCT_FAILURE`,
}

export const initialState = {
    selectedProductId: '',
    product: {
        name: '',
        description: '',
        amount: '',
        price: '',
        photoUrl: ''
    },
    products: []
}



export function reducer(state = initialState, action) {
    switch (action.type) {
        case types.GET_PRODUCTS_SUCCESS:
            return {
                ...state,
                products: action.payload.products
            }
        case types.SELECTED_PRODUCT_SET:
            return{
                ...state,
                selectedProductId: action.payload
            }
        case types.FIND_PRODUCT:
        return{
                ...state,
                product: action.payload
            }
        case types.GET_PRODUCT_SUCCESS:
        return{
                ...state,
                product: action.payload
            }
        default:
            return { ...state }
    }
}