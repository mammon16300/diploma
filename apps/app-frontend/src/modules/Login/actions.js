import { types } from './reducer'
import { RSAA } from 'redux-api-middleware'
import Cookies from 'universal-cookie'
import { actions as rootActions } from '../Root/actions'
const cookies = new Cookies()

const host = process.env.HOST || 'app-gw'

const entryPoint = `http://${host}/api`


export const actions = {
    logIn: (email, password) => dispatch => dispatch({ 
        [RSAA]: {
            types: [
                {
                    type: types.LOGIN_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.LOGIN_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                         
                            cookies.set('token',data.token,{path: '/'})
                            cookies.set('token',data.token,{path: '/',domain: 'http://app-gw/'})
                            dispatch(rootActions.getCurrentUser())
                            return data
                        })

                },
                {
                    type: types.LOGIN_FAILURE,
                    payload: (action, state, res) => {
                        return 
                    }

                }
            ],
            endpoint: `${entryPoint}/login`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({ email, password }),
            headers: { 'Content-Type': 'application/json' }
        }
    })
}