import React from 'react'
import qs from 'querystring'
import { SessionTable } from './SessionTable'
import GymSessionsList from '../container/GymSessionsList'

import {
    Route,
    Switch,
    useRouteMatch,
    Link
}
    from "react-router-dom"
function SwitchPanel(props) {
    const { uuid } = props
    let { path, url } = useRouteMatch();    

    return (
      <Switch>
      <Route exact path={path}>
      <SessionTable
        activeSessions={props.activeSessions}
        closeSession={props.closeSession}
        setCurrentUser={props.setCurrentUser}
      />
      </Route> 
      <Route exact path={`${path}/registernew`}>
      <GymSessionsList/>
      </Route> 
      </Switch>
    )
}
export default SwitchPanel