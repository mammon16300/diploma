import React from 'react'
import { Link } from "react-router-dom"
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'

const dateFormatter = (date) => {
  return Intl.DateTimeFormat('uk', {
    year: 'numeric',
    month: 'short',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit'
  }).format(new Date(date))
}

export const SessionTable = (props) => {
  const {
    activeSessions,
    closeSession,
    setCurrentUser
  } = props

  const closeSessionButton = (value, row) => {
    return <button onClick={() => closeSession(row.uuid)}>Завершити</button>
  }
  const goToProfile = (value, row) => {
    return <Link
      to={`/user/list/userinfo`}
      onClick={() => setCurrentUser(row.userId)}
    >Перейти</Link>
  }

  return (
    <div className='emp-content'>
      <BootstrapTable
        data={activeSessions}
        striped hover
      >
        <TableHeaderColumn isKey dataField='uuid' hidden></TableHeaderColumn>
        <TableHeaderColumn dataField='fullUserName'>Клієнт</TableHeaderColumn>
        <TableHeaderColumn dataField='locker'>Номер шкафчика</TableHeaderColumn>
        <TableHeaderColumn dataField='startDate' dataFormat={dateFormatter}>Час входу</TableHeaderColumn>
        <TableHeaderColumn dataField="button" dataFormat={goToProfile}>Перейти в профіль</TableHeaderColumn>
        <TableHeaderColumn dataField="button" dataFormat={closeSessionButton}>Закрити сесію</TableHeaderColumn>
      </BootstrapTable>
    </div>
  )
}