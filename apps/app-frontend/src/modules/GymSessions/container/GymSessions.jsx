import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'

import { NAME, actions } from '..'
import { SessionTable } from '../components/SessionTable'
import SwitchPanel from '../components/SwithPanel'
import { actions as usersActions } from  '../../Users/actions'

function mapStateToProps(state) {
  return {
    ...state[NAME]
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...actions,
  ...usersActions
}, dispatch)


class GymSessions extends Component {
  constructor(props) {
    super(props)
  }
  componentDidMount() {
    const { getActiveSessions } = this.props
    getActiveSessions()
  }

  render() {
    const {
      activeSessions,
      closeSession
    } = this.props
    console.log('props',this.props)
    return (
        <SwitchPanel
          activeSessions={activeSessions}
          closeSession={closeSession}
          setCurrentUser={this.props.setCurrentUser}
        />
    )
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(GymSessions)
