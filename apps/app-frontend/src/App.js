import React from 'react';
import logo from './logo.svg';
import Root from './modules/Root/container/Root'
import './App.css';
//import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <Root/>
  );
}

export default App;
