<div
className="sidebar"
//    data-color={this.props.bgColor}
//    data-active-color={this.props.activeColor}
>
<div className="logo">
  <a
    href="https://www.creative-tim.com"
    className="simple-text logo-mini"
  >
  </a>
  <a
    href="https://www.creative-tim.com"
    className="simple-text logo-normal"
  >
    Creative Tim
  </a>
</div>
<div className="sidebar-wrapper ps">
  <Nav>
    {this.props.routes.map((route, index) => {
      return (
        <li
          className=''
          key={index}
        >
          <NavLink
            to={route.layout + route.path}
            className="nav-link"
            activeClassName="active"
          >
            <i className={route.icon} />
            <p>{route.name}</p>
          </NavLink>
        </li>
      );
    })}
  </Nav>
</div>
</div>








<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
   <!--Made with love by Mutiullah Samim -->
   
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>Sign In</h3>
				
			</div>
			<div class="card-body">
				<form>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" class="form-control" placeholder="username">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" placeholder="password">
					</div>
					<div class="row align-items-center remember">
						<input type="checkbox">Remember Me
					</div>
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Don't have an account?<a href="#">Sign Up</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="#">Forgot your password?</a>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>















<div className="container">
                <h1 className="well">Регистрация пользователя</h1>
                <div className="col-lg-12 well">
                    <div className="row">
                        <form>
                            <div className="col-sm-12">
                                <div className="row">
                                    <div className="col-sm-6 form-group">
                                        <label>Имя</label>
                                        <input className="form-control" type="text" placeholder="Введите имя сюда.." />
                                    </div>
                                    <div className="col-sm-6 form-group">
                                        <label>Фамилия</label>
                                        <input className="form-control" type="text" placeholder="Введите фамилию сюда.." />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Адрес</label>
                                    <textarea className="form-control" placeholder="Введите адрес сюда.." rows={3} defaultValue={""} />
                                </div>
                                <div className="row">
                                    <div className="col-sm-4 form-group">
                                        <label>City</label>
                                        <input className="form-control" type="text" placeholder="Enter City Name Here.." />
                                    </div>
                                    <div className="col-sm-4 form-group">
                                        <label>State</label>
                                        <input className="form-control" type="text" placeholder="Enter State Name Here.." />
                                    </div>
                                    <div className="col-sm-4 form-group">
                                        <label>Zip</label>
                                        <input className="form-control" type="text" placeholder="Enter Zip Code Here.." />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6 form-group">
                                        <label>Title</label>
                                        <input className="form-control" type="text" placeholder="Enter Designation Here.." />
                                    </div>
                                    <div className="col-sm-6 form-group">
                                        <label>Company</label>
                                        <input className="form-control" type="text" placeholder="Enter Company Name Here.." />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label>Phone Number</label>
                                    <input className="form-control" type="text" placeholder="Enter Phone Number Here.." />
                                </div>
                                <div className="form-group">
                                    <label>Email Address</label>
                                    <input className="form-control" type="text" placeholder="Enter Email Address Here.." />
                                </div>
                                <div className="form-group">
                                    <label>Website</label>
                                    <input className="form-control" type="text" placeholder="Enter Website Name Here.." />
                                </div>
                                <button className="btn btn-lg btn-info" type="button">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>