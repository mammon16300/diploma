import response from '../helpers/response'

import authService from '../services/auth'
import salesService from '../services/sales'


export const getUserBalance = async (req, res, next) => {
  try {
    const balance = await salesService.getBalance(req.query.userId)

    response.ok(res, balance)
  } catch (e) {
    console.log('getUserBalance e', e)
    next(e)
  }
}

export const updateUserBalance = async (req, res, next) => {
  try {
    const { userId } = req.params
    const { amount } = req.body

    const balance = await salesService.updateBalance(userId, { amount })

    response.ok(res, balance)
  } catch(e) {
    console.log('updateUserBalance e',e)
    next(e)
  }
}