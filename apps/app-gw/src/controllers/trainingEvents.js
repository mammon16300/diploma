import _ from 'lodash'
import moment from 'moment'

import response from '../helpers/response'

import workoutService from '../services/workout'
import salesService from '../services/sales'
import authService from '../services/auth'


export const createTrainingEvent = async (req, res, next) => {
  try {
    const allowedProps = ['start', 'end', 'userId', 'coachId', 'type']
    const requestData = _.pick(req.body, allowedProps)

    let balance
    if (requestData.type === 'duo') {
      balance = await salesService.getBalance(requestData.userId)

      if (balance.personalTrainings < 1) return response.error(res, 'LackOfPersonalTrainings', 409)

      balance = await salesService.updateBalance(requestData.userId, { personalTrainings: -1 })
    }

    const trainingEvent = await workoutService.createTrainingEvent(requestData)

    const user = await authService.getUser({ uuid: requestData.userId })
    trainingEvent.fullName = user.firstName + ' ' + user.lastName

    response.ok(res, trainingEvent, 201)
  } catch (e) {
    console.log('createTrainingEvent error', e)
    next(e)
  }
}

// export const updateTrainingEvent = async (req, res, next) => {
//   try {
//     const { uuid } = req.params
//     const allowedProps = ['date', 'startTime', 'endTime', 'userId', 'coachId']

//     const trainingEvent = await workoutService.updateTrainingEvent(uuid, _.pick(req.body, allowedProps))

//     response.ok(res, trainingEvent, 201)
//   } catch (e) {
//     console.log('updateTrainingEvent error', e)
//     next(e)
//   }
// }

export const getTrainingEvents = async (req, res, next) => {
  try {
    const allowedProps = ['uuid', 'userId', 'coachId', 'dateRange', 'type']
    const searchTerms = _.pick(req.query, allowedProps)

    if (searchTerms.dateRange) {
      if (searchTerms.dateRange.from) searchTerms.start = { '$gte': new Date(searchTerms.dateRange.from) }
      if (searchTerms.dateRange.to) searchTerms.end = { '$lte': new Date(searchTerms.dateRange.to) }

      delete searchTerms.dateRange
    }

    const trainingEvents = await workoutService.getTrainingEvents(searchTerms)

    const users = await authService.getUsers({ uuid: { $in: trainingEvents.map(event => event.userId) } })
    trainingEvents.forEach(event => {
      const user = users.find(user => user.uuid === event.userId)
      event.fullName = user.firstName + ' ' + user.lastName
    })

    response.ok(res, trainingEvents, 200)
  } catch (e) {
    console.log('getTrainingEvents error', e)
    next(e)
  }
}

export const declineTrainingEvent = async (req, res, next) => {
  try {
    let trainingEvent = (await workoutService.getTrainingEvents({ uuid: req.body.uuid }))[0]

    let isCompensated = true

    let isCanceledInTime = moment.duration(moment(trainingEvent.start).diff(moment())).asHours() >= 3
    if (req.user.role === 'user' && !isCanceledInTime) isCompensated = false

    if (isCompensated) await salesService.updateBalance(trainingEvent.userId, { personalTrainings: 1 })

    trainingEvent = await workoutService.updateTrainingEvent(trainingEvent.uuid, { declinedBy: req.user.id })

    const user = await authService.getUser({ uuid: trainingEvent.userId })
    trainingEvent.fullName = user.firstName + ' ' + user.lastName

    response.ok(res, trainingEvent)
  } catch (e) {
    console.log('declineTrainingEvent e', e)
    next(e)
  }
}

