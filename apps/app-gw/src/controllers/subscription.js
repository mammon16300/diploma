import _ from 'lodash'

import response from '../helpers/response'

import salesService from '../services/sales'
import workoutService from '../services/workout'
import moment from 'moment'


export const createSubscription = async (req, res, next) => {
  try {
    const allowedProps = ['name', 'description', 'price']

    const subscription = await salesService.createSubscription(_.pick(req.body, allowedProps))

    response.ok(res, subscription, 201)
  } catch (e) {
    console.log('createSubscription error', e)
    next(e)
  }
}

export const updateSubscription = async (req, res, next) => {
  try {
    const { uuid } = req.params
    const allowedProps = ['name', 'description', 'price']
    console.log(1)
    const subscription = await salesService.updateSubscription(uuid, _.pick(req.body, allowedProps))
    console.log(12)
    response.ok(res, subscription, 201)
  } catch (e) {
    console.log('updateSubscription error', e)
    next(e)
  }
}

export const getSubscriptions = async (req, res, next) => {
  try {
    const allowedProps = ['name', 'uuid']
    const subscriptions = await salesService.getSubscriptions(_.pick(req.query, allowedProps))

    response.ok(res, subscriptions, 200)
  } catch (e) {
    console.log('getSubscriptions error', e)
    next(e)
  }
}


export const deleteSubscription = async (req, res, next) => {
  try {
    const subscription = await salesService.deleteSubscription(req.query.uuid)

    response.ok(res, subscription)
  } catch (e) {
    console.log('deleteSubscription error', e)
    next(e)
  }
}

export const getUserSubscriptions = async (req, res, next) => {
  try {
    const { userId } = req.query

    const userSubscriptions = await workoutService.getUserSubscriptions({
      $or: [
        { userId, isPaused: true },
        { userId, endDate: { $gt: new Date().toJSON() } }
      ]
    })

    const orders = await salesService.getOrders({
      userId,
      'product.uuid': { $in: userSubscriptions.map(item => item.subscriptionId) }
    })

    const data = []
    
    let order
    userSubscriptions.map(subscription => {
      order = orders.find(item => item.product.uuid === subscription.subscriptionId)
      data.push({
        userId,
        ..._.pick(subscription, ['endDate', 'isPaused', 'pauseDate', 'startDate', 'uuid']),
        subscription: _.pick(order.product, ['name', 'description'])
      })
    })

    response.ok(res, data)
  } catch (e) {
    console.log('getUserSubscriptions error', e)
    next(e)
  }
}

export const pauseUserSubscription = async (req, res, next) => {
  try {
    const { uuid: userSubscriptionId } = req.params
    let [subscription] = await workoutService.getUserSubscriptions({ uuid: userSubscriptionId })

    if (!subscription) return response.error(res, 'NoUserSubscription', 400)
    if (subscription.isPaused) return response.ok(res, subscription)

    subscription = await workoutService.updateUserSubscription(
      req.params.uuid,
      {
        isPaused: true,
        pauseDate: moment().startOf('day')
      }
    )

    response.ok(res, subscription)
  } catch (e) {
    console.log('pauseUserSubscriptions error', e)
    next(e)
  }
}

export const resumeUserSubscription = async (req, res, next) => {
  try {
    const { uuid: userSubscriptionId } = req.params
    let [subscription] = await workoutService.getUserSubscriptions({ uuid: userSubscriptionId })

    if (!subscription) return response.error(res, 'NoUserSubscription', 400)
    if (!subscription.isPaused) return response.ok(res, subscription)

    const getNewEndDate = (subscription) => {
      const endDate = moment(subscription.endDate)
      const pauseDate = moment(subscription.pauseDate)

      const daysLeft = endDate.diff(pauseDate, 'days')

      return moment().endOf('day').add(daysLeft, 'days').toISOString()
    }

    subscription = await workoutService.updateUserSubscription(
      req.params.uuid,
      {
        isPaused: false,
        endDate: getNewEndDate(subscription),
        pauseDate: ''
      }
    )

    response.ok(res, subscription)
  } catch (e) {
    console.log('pauseUserSubscriptions error', e)
    next(e)
  }
}