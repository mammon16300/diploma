import _ from 'lodash'

import response from '../helpers/response'
import workoutService from '../services/workout'
import authService from '../services/auth'

export const registerUserGymSession = async (req, res, next) => {
  try {
    const allowedProps = ['userId', 'locker']
    const session = await workoutService.createGymSession(_.pick(req.body, allowedProps))

    const user = await authService.getUser({ uuid: req.body.userId })
    session.fullUserName = `${user.firstName} ${user.lastName}`

    response.ok(res, session, 201)
  } catch (e) {
    console.log('registerUserGymSession error', e)
    next(e)
  }
}

export const getActiveGymSessions = async (req, res, next) => {
  try {
    const sessions = await workoutService.getGymSessions({ endDate: null })

    const users = await authService.getUsers()

    sessions.forEach(element => {
      const user = users.find(item => item.uuid === element.userId)
      
      element.fullUserName = `${user.firstName} ${user.lastName}`
      //delete element.userId
      delete element.endDate
    })

    response.ok(res, sessions, 200)
  } catch (e) {
    console.log('getActiveGymSessions error', e)
    next(e)
  }
}

export const closeUserGymSession = async (req, res, next) => {
  try {
    const session = await workoutService.updateGysession(req.body.uuid, { endDate: new Date() })

    response.ok(res, session)
  } catch (e) {
    console.log('closeUserGymSession error', e)
    next(e)
  }
}
