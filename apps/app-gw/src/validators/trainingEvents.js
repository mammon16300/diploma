import { body, param, query, header, validationResult, oneOf, check } from 'express-validator'
import validatorsCheck from './validatorsCheck'

import authService from '../services/auth'
import workoutService from '../services/workout'

const validator = {}

validator.create = [
  body(['start', 'end']).isISO8601(),
  oneOf([
    [
      header('_role').isIn(['user', 'coach']),
      body('type').equals('solo'),
      body('userId').not().exists().customSanitizer((value, { req }) => req.user.id),
      body('coachId').not().exists()
    ],
    [
      header('_role').equals('coach'),
      body('type').equals('duo'),

      body('userId').isUUID().custom(async (value, { req }) => {
        const user = await authService.getUser({ uuid: value })
        if (user.defaultCoachId === req.user.id) return Promise.resolve()
        return Promise.reject()
      }),
      body('coachId').not().exists().customSanitizer((value, { req }) => {
        return req.user.id
      })
    ],
    [
      header('_role').isIn(['manager', 'admin']),
      body('type').equals('solo'),
      body('coachId').not().exists(),
      body('userId').isUUID()
    ],
    [
      header('_role').isIn(['manager', 'admin']),
      body('type').equals('duo'),
      body('userId').isUUID(),
      body('coachId').isUUID()
    ]
  ]),
  validatorsCheck
]

validator.get = [
  query(['dateRange[from]', 'dateRange[to]']).optional().isISO8601(),
  oneOf([
    [
      header('_role').isIn(['user', 'coach']),
      query('type').optional().isIn(['solo', 'duo']),
      query('userId').not().exists().customSanitizer((value, { req }) => req.user.id),
      query('coachId').not().exists()
    ],
    [
      header('_role').equals('coach'),
      query('type').equals('duo'),
      query('userId').optional().custom(async (value, { req }) => {
        const user = await authService.getUser({ uuid: value })
        return user.defaultCoachId === req.user.id
          ? Promise.resolve()
          : Promise.reject()
      }),
      query('coachId').not().exists().customSanitizer((value, { req }) => req.user.id)
    ],
    [
      header('_role').isIn(['manager', 'admin']),
      query('type').optional().isIn(['solo', 'duo']),
      query('userId').optional().isUUID(),
      query('coachId').optional().isUUID()
    ]
  ]),
  validatorsCheck
]

// user can decline in 3 hours 
// coach can decline
// manager decline anytime 
// payback always
validator.declineTrainingEvent = [
  body('uuid').isUUID(),
  oneOf([
    [
      header('_role').isIn(['user', 'coach']),
      body('uuid').custom(async (value, { req }) => {
        const event = (await workoutService.getTrainingEvents({ uuid: value }))[0]
        return event.userId === req.user.id
          ? Promise.resolve()
          : Promise.reject()
      })
    ],
    [
      header('_role').equals('coach'),
      body('uuid').custom(async (value, { req }) => {
        const event = (await workoutService.getTrainingEvents({ uuid: value }))[0]
        return event.coachId === req.user.id
          ? Promise.resolve()
          : Promise.reject()
      })
    ],
    [
      header('_role').isIn(['manager', 'admin'])
    ]
  ]),
  validatorsCheck
]


export default validator
