import { body, param, query, header, validationResult, oneOf, check } from 'express-validator'
import validatorsCheck from './validatorsCheck'

const validator = {}

validator.purchaseSubscription = [
  body('productId').isUUID(),
  body('amount').isInt({ min: 1 }),
  oneOf([
    [
      header('_role').isIn(['user', 'coach']),
      body('userId').not().exists().customSanitizer((value, { req }) => req.user.id),
      body('isCash').not().exists()
    ],
    [
      header('_role').isIn(['admin', 'manager']),
      body('userId').isUUID(),
      body('isCash').isBoolean()
    ]
  ], ('roleAccessDenied')),
  validatorsCheck
]

validator.purchaseProduct = [
  body('productId').isUUID(),
  body('amount').isInt({ min: 1 }),
  body('userId').optional().isUUID(),
  body('isCash').isBoolean(),
  validatorsCheck
]

validator.purchasePersonalTrainings = [
  body('amount').isInt({ min: 1 }),
  oneOf([
    [
      header('_role').equals('user'),
      body('userId').not().exists().customSanitizer((value, { req }) => req.user.id),
      body('isCash').not().exists()
    ],
    [
      header('_role').isIn(['manager', 'admin']),
      body('userId').isUUID(),
      body('isCash').isBoolean()
    ]
  ])
]

export default validator
