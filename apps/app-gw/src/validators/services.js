import { body, param, query, header, validationResult, oneOf, check } from 'express-validator'
import validatorsCheck from './validatorsCheck'

const validator = {}

validator.createService = [
  body('name').isString().isLength({ min: 1 }),
  body('price').isFloat({ min: 0.01 }),
  validatorsCheck
]

validator.updateService = [
  param('uuid').isUUID(),
  body('name').optional().isString().isLength({ min: 1 }),
  body('price').optional().isFloat({ min: 0.01 }),
  oneOf([
    body('name').exists(),
    body('price').exists()
  ]),
  validatorsCheck
]

validator.deleteService = [
  param('uuid').isUUID(),
  validatorsCheck
]

validator.getServices = [
  query('uuid').optional().isUUID(),
  validatorsCheck
]

validator.updatePersonalTrainingService = [
  body('amount').optional().isFloat({ min: 0.01 }),
  body('name').optional().isString()
]

export default validator
