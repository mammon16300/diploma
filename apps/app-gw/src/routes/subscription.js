import express from 'express'

import {
  createSubscription,
  deleteSubscription,
  getSubscriptions,
  updateSubscription,
  getUserSubscriptions,
  pauseUserSubscription,
  resumeUserSubscription
} from '../controllers/subscription'
import roleAccessMiddleware from '../helpers/roleAccessMiddleware'
import requestValidator from '../validators/subscriptions'

const router = express.Router()

router.route('/subscriptions').post(
  roleAccessMiddleware('manager'),
  requestValidator.createSubscription,
  createSubscription
)

router.route('/subscriptions').delete(
  roleAccessMiddleware('manager'),
  requestValidator.deleteSubscriptions,
  deleteSubscription
)

router.route('/subscriptions/:uuid').patch(
  roleAccessMiddleware('manager'),
  requestValidator.updateSubscription,
  updateSubscription
)

router.route('/subscriptions').get(
  requestValidator.getSubscriptions,
  getSubscriptions
)

router.route('/userSubscriptions').get(
  requestValidator.getUserSubscriptions,
  getUserSubscriptions
)

router.route('/pauseUserSubscription/:uuid').patch(
  roleAccessMiddleware('manager'),
  requestValidator.pauseUserSubscription,
  pauseUserSubscription
)

router.route('/resumeUserSubscription/:uuid').patch(
  roleAccessMiddleware('manager'),
  requestValidator.resumeUserSubscription,
  resumeUserSubscription
)

export default router
