import express from 'express'

import {
  purchaseSubscription,
  purchaseProduct,
  purchasePersonalTrainings
} from '../controllers/orders'
import roleAccessMiddleware from '../helpers/roleAccessMiddleware'
import ordersValidator from '../validators/orders'

const router = express.Router()

router.route('/purchase/subscription').post(
  ordersValidator.purchaseSubscription,
  purchaseSubscription
)

router.route('/purchase/product').post(
  roleAccessMiddleware('manager'),
  ordersValidator.purchaseProduct,
  purchaseProduct
)

router.route('/purchase/personalTrainings').post(
  ordersValidator.purchasePersonalTrainings,
  purchasePersonalTrainings
)

export default router
