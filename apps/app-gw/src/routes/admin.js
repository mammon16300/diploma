import express from 'express'

import {
  getRevenue,
  getVisists,
  getRevenueReport
} from '../controllers/admin'

import roleAccessMiddleware from '../helpers/roleAccessMiddleware'
import validator from '../validators/admin'

const router = express.Router()

router.route('/revenue').get(
  roleAccessMiddleware('admin'),
  validator.getRevenue,
  getRevenue
)

router.route('/visits').get(
  roleAccessMiddleware('admin'),
  validator.getVisists,
  getVisists
)

router.route('/revenueReport').get(
  roleAccessMiddleware('admin'),
  validator.getRevenueReport,
  getRevenueReport
)

export default router
