import express from 'express'

import {
  getUserBalance,
  updateUserBalance
} from '../controllers/balance'
import roleAccessMiddleware from '../helpers/roleAccessMiddleware'
import validator from '../validators/balance'

const router = express.Router()

router.route('/balance').get(
  roleAccessMiddleware('manager'),
  validator.getBalance,
  getUserBalance
)

router.route('/balance/:userId').patch(
  roleAccessMiddleware('manager'),
  validator.updateBalance,
  updateUserBalance
)


export default router
