import authService from '../services/auth'

const userAuthorizationMiddleware = async (req, res, next) => {
  try {
    if (!req.cookies.token) return next({ type: 'AuthorizationFailed' })

    const user = await authService.getCurrentUser({ token: req.cookies.token })
    req.user = {
      id: user.uuid,
      role: user.role
    }

    req.headers['_role'] = user.role

    next()
  } catch (e) {
    next(e)
  }
}

export default userAuthorizationMiddleware