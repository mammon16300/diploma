import axios from 'axios'
import qs from 'qs'

class WorkoutService {
  constructor() {
    this._requestTimeout = 10000
    this._host = 'http://service-workout'
  }

  async _patch(endpoint, uuid, body = {}) {
    try {
      const response = await axios.patch(this._host + endpoint + '/' + uuid, body, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async _post(endpoint, body = {}) {
    try {
      const response = await axios.post(this._host + endpoint, body, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async _get(endpoint, query = {}) {
    const queryString = qs.stringify(query)
    try {
      const response = await axios.get(this._host + endpoint + '?' + queryString, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async _delete(endpoint, query = {}) {
    const queryString = qs.stringify(query)
    try {
      const response = await axios.delete(this._host + endpoint + '?' + queryString, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async testEndpointCall() {
    return axios.get(this._host + '/', { timeout: this._requestTimeout })
  }

  async addUserSubscription(body) {
    return this._post('/userSubscriptions', body)
  }

  async getUserSubscriptions(query) {
    return this._get('/userSubscriptions', query)
  }

  async updateUserSubscription(uuid, body) {
    return this._patch('/userSubscriptions', uuid, body)
  }

  async createTrainingEvent(body) {
    return this._post('/trainingEvents', body)
  }

  async getTrainingEvents(query) {
    return this._get('/trainingEvents', query)
  }

  async updateTrainingEvent(uuid, body) {
    return this._patch('/trainingEvents', uuid, body)
  }

  async createGymSession(body) {
    return this._post('/gymSessions', body)
  }

  async getGymSessions(query) {
    return this._get('/gymSessions', query)
  }

  async updateGysession(uuid, body) {
    return this._patch('/gymSessions', uuid, body)
  }
}

export default new WorkoutService()