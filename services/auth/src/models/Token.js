import mongoose from 'mongoose'

const TokenSchema = new mongoose.Schema({
  uuid: { type: String, required: true },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  //authMethod: { type: String, required: true, enum: Config.common.availableAuthMethods },
  createdAt: { type: Date, default: Date.now }
})

TokenSchema.index({ createdAt: 1 }, { name: 'createdAtIndex', expires: '30d' })
TokenSchema.index({ uuid: 1 }, { name: 'uuidIndex', unique: true })

export default mongoose.model('Token', TokenSchema)
