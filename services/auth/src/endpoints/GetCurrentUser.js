import express from 'express'
import response from '../helpers/response'

import TokenModel from '../models/Token'

const DUPLICATE_KEY_ERROR_CODE = 11000

const GetCurrentUser = express.Router()

GetCurrentUser.get('/currentUser',
  async (req, res, next) => {
    try {
      const token = await TokenModel.findOne({ uuid: req.query.token }).populate('user').lean()

      if (!token) {
        return response.error(res, 'AuthorizationFailed', 401)
      }

      response.ok(res, token.user)
    } catch (e) {
      console.log('GetUser e', e)
      next(e)
    }
  }
)

export default GetCurrentUser