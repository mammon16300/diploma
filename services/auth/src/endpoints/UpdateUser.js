import express from 'express'
import sha1 from 'sha1'
import _ from 'lodash'
import response from '../helpers/response'

import UserModel from '../models/User'

const DUPLICATE_KEY_ERROR_CODE = 11000

const UpdateUser = express.Router()

UpdateUser.patch('/user/:uuid',
  async (req, res, next) => {
    try {
      const {
        params: {
          uuid
        },
        body
      } = req

      const newProperties = { ...body }

      if (newProperties.password) newProperties.password = sha1(newProperties.password)

      const user = await UserModel.findOne({ uuid })

      if (!user || !uuid) {
        return response.ok(res, {}, 204)
      }

      Object.keys(newProperties).map(key => {
        user[key] = newProperties[key]
      })

      await user.save()

      response.ok(res, user)
    } catch (e) {
      console.log('UpdateUser e', e)
      next(e)
    }
  }
)

export default UpdateUser