import express from 'express'
import _ from 'lodash'
import response from '../helpers/response'

import UserModel from '../models/User'

const DUPLICATE_KEY_ERROR_CODE = 11000

const GetUser = express.Router()

GetUser.get('/user',
  async (req, res, next) => {
    try {
      const user = await UserModel.findOne(req.query)

      response.ok(res, user)
    } catch (e) {
      console.log('GetUser e', e)
      next(e)
    }
  }
)

export default GetUser