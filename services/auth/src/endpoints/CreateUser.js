import express from 'express'
import sha1 from 'sha1'
import _ from 'lodash'
import response from '../helpers/response'

import UserModel from '../models/User'

const DUPLICATE_KEY_ERROR_CODE = 11000

const CreateUser = express.Router()

CreateUser.post('/user',
  async (req, res, next) => {
    try {
      const userData = { ...req.body }
      userData.password = sha1(userData.password)

      const user = await UserModel.create(userData)

      response.ok(res, user, 201)
    } catch (e) {
      console.log(e)
      console.log('createUser e', e)
      if (e.code === DUPLICATE_KEY_ERROR_CODE) {
        response.error(res, 'DuplicateKeyError', 409, { key: Object.keys(e.keyPattern)[0] })
      } else {
        next(e)
      }
    }
  }
)

export default CreateUser
