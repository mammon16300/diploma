import app from './app'
import mongoose from 'mongoose'


async function start() {
  try {
    await mongoose.connect(process.env.MONGO_SERVER, { useNewUrlParser: true, useCreateIndex: true })

  } catch (e) {
    console.log('error', 'Database connection error', e)
    process.exit(1)
  }
  console.log('workout service',process.env.NODE_PORT)
  try {
    app.listen(process.env.NODE_PORT || 80)
  } catch (e) {
    console.log('error', 'Startup error', e)
  }
}

start().then(() => {
  console.log('info', 'Started on ', process.env.NODE_PORT)
})
