import express from 'express'

import response from '../../helpers/response'
import UserSubscriptions from '../../models/UserSubscriptions'

const AddUserSubscription = express.Router()

AddUserSubscription.post('/userSubscriptions',
  async (req, res, next) => {
    try {
      const subscription = await UserSubscriptions.create(req.body)

      response.ok(res, subscription)
    } catch (e) {
      console.log('AddUserSubscription e', e)
      next(e)
    }
  }
)

export default AddUserSubscription
