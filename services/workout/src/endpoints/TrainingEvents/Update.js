import express from 'express'

import response from '../../helpers/response'
import TrainingEvent from '../../models/TrainingEvent'

const Update = express.Router()

Update.patch('/trainingEvents/:uuid',
  async (req, res, next) => {
    try {
      const trainingEvent = await TrainingEvent.findOneAndUpdate(
        { uuid: req.params.uuid },
        req.body,
        { new: true }
      )

      response.ok(res, trainingEvent)
    } catch (e) {
      console.log('TrainingEvent update e', e)
      next(e)
    }
  }
)

export default Update
