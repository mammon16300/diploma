import express from 'express'

import response from '../../helpers/response'
import GymSession from '../../models/GymSession'

const Create = express.Router()

Create.post('/gymSessions',
  async (req, res, next) => {
    try {
      const session = await GymSession.create(req.body)

      response.ok(res, session)
    } catch (e) {
      console.log('GymSession create e', e)
      next(e)
    }
  }
)

export default Create
