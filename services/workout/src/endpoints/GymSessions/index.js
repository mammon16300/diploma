import express from 'express'

import Create from './Create'
import Get from './Get'
import Update from './Update'

const router = express.Router()

router.use(Create)
router.use(Get)
router.use(Update)

export default router
