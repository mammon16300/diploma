import express from 'express'

import response from '../../helpers/response'
import GymSession from '../../models/GymSession'

const Update = express.Router()

Update.patch('/gymSessions/:uuid',
  async (req, res, next) => {
    try {
      const session = await GymSession.findOneAndUpdate(
        { uuid: req.params.uuid },
        req.body,
        { new: true }
      )

      response.ok(res, session)
    } catch (e) {
      console.log('GymSession update e', e)
      next(e)
    }
  }
)

export default Update
