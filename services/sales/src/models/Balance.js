import mongoose from 'mongoose'

const BalanceSchema = new mongoose.Schema({
  userId: { type: String, required: true },
  balance: { type: Number, default: 0 },
  personalTrainings: { type: Number, default: 0 }
})

BalanceSchema.index({ userId: 1 }, { name: 'userIdIndex', unique: true })

BalanceSchema.methods.toJSON = function () {
  var obj = this.toObject()
  
  delete obj._id
  delete obj.__v
  
  return obj
}

export default mongoose.model('Balance', BalanceSchema)
