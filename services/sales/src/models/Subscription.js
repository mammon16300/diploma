import mongoose from 'mongoose'
import { v1 as uuid } from 'uuid'

const SubscriptionSchema = new mongoose.Schema({
  uuid: { type: String, required: true, default: uuid },
  name: { type: String, required: true }, 
  description: { type: String },
  price: { type: Number, required: true }
})

SubscriptionSchema.index({ uuid: 1 }, { name: 'uuidIndex', unique: true })

SubscriptionSchema.methods.toJSON = function () {
  var obj = this.toObject()

  delete obj._id
  delete obj.__v

  return obj
}

export default mongoose.model('Subscription', SubscriptionSchema)
