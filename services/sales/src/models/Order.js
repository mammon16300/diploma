import mongoose from 'mongoose'
import { v1 as uuid } from 'uuid'

const OrderSchema = new mongoose.Schema({
  uuid: { type: String, required: true, default: uuid },
  userId: { type: String, default: null },

  productType: { type: String, required: true, enum: ['product', 'subscription', 'service'] },
  product: { type: Object, required: true },

  amount: { type: Number, default: 1, required: true },
  date: { type: Date, required: true, default: Date.now }
})

OrderSchema.index({ uuid: 1 }, { name: 'uuidIndex', unique: true })

OrderSchema.methods.toJSON = function () {
  var obj = this.toObject()

  delete obj._id
  delete obj.__v

  return obj
}

export default mongoose.model('Order', OrderSchema)
