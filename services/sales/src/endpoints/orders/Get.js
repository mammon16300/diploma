import express from 'express'

import response from '../../helpers/response'
import OrderModel from '../../models/Order'

const Get = express.Router()

Get.get('/orders',
  async (req, res, next) => {
    try {
      const orders = await OrderModel.find(req.query)

      response.ok(res, orders)
    } catch (e) {
      console.log('Get orders e', e)
      next(e)
    }
  }
)

export default Get
