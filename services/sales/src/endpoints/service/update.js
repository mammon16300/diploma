import express from 'express'

import response from '../../helpers/response'
import ServiceModel from '../../models/Service'

const Update = express.Router()

Update.patch('/services/:uuid',
  async (req, res, next) => {
    try {
      const { uuid } = req.params
      
      const service = await ServiceModel.findOneAndUpdate({ uuid }, req.body, { new: true })
      
      response.ok(res, service, 201)
    } catch (e) {
      console.log('UpdateService e', e)
      next(e)
    }
  }
)

export default Update
