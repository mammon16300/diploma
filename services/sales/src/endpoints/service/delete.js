import express from 'express'

import response from '../../helpers/response'
import ServiceModel from '../../models/Product'

const Delete = express.Router()

Delete.delete('/services',
  async (req, res, next) => {
    try {
      const service = await ServiceModel.findOneAndDelete({ uuid: req.query.uuid })

      response.ok(res, service)
    } catch (e) {
      console.log('DeleteService e', e)
      next(e)
    }
  }
)

export default Delete
