import express from 'express'

import Create from './create'
import Delete from './delete'
import Get from './get'
import Update from './update'

const router = express.Router()

router.use(Create)
router.use(Delete)
router.use(Get)
router.use(Update)

export default router
