import express from 'express'

import response from '../../helpers/response'
import ServiceModel from '../../models/Service'

const Get = express.Router()

Get.get('/services',
  async (req, res, next) => {
    try {
      const services = await ServiceModel.find(req.query)

      response.ok(res, services)
    } catch (e) {
      console.log('GetServices e', e)
      next(e)
    }
  }
)

export default Get
