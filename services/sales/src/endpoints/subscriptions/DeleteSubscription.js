import express from 'express'

import response from '../../helpers/response'
import SubscriptionModel from '../../models/Subscription'

const DeleteSubscription = express.Router()

DeleteSubscription.delete('/subscriptions',
  async (req, res, next) => {
    try {
      const subscription = await SubscriptionModel.findOneAndDelete({ uuid: req.query.uuid })

      response.ok(res, subscription)
    } catch (e) {
      console.log('DeleteSubscription e', e)
      next(e)
    }
  }
)

export default DeleteSubscription
