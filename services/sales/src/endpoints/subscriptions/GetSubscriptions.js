import express from 'express'

import response from '../../helpers/response'
import SubscriptionModel from '../../models/Subscription'

const GetSubscriptions = express.Router()

GetSubscriptions.get('/subscriptions',
  async (req, res, next) => {
    try {
      const subscriptions = await SubscriptionModel.find(req.query)

      response.ok(res, subscriptions)
    } catch (e) {
      console.log('DeleteSubscription e', e)
      next(e)
    }
  }
)

export default GetSubscriptions
