import express from 'express'

import response from '../../helpers/response'
import SubscriptionModel from '../../models/Subscription'

const UpdateSubscription = express.Router()

UpdateSubscription.patch('/subscriptions/:uuid',
  async (req, res, next) => {
    try {
      const { uuid } = req.params
      
      const subscription = await SubscriptionModel.findOneAndUpdate({ uuid }, req.body, { new: true })
      
      response.ok(res, subscription, 201)
    } catch (e) {
      console.log('UpdateSubscription e', e)
      next(e)
    }
  }
)

export default UpdateSubscription
