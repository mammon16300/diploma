import express from 'express'

import response from '../../helpers/response'
import ProductModel from '../../models/Product'

const Get = express.Router()

Get.get('/products',
  async (req, res, next) => {
    try {
      const products = await ProductModel.find(req.query)

      response.ok(res, products)
    } catch (e) {
      console.log('GetProducts e', e)
      next(e)
    }
  }
)

export default Get
